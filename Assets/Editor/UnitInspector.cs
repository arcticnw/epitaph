﻿//using UnityEditor;
//using UnityEngine;
//using System.Collections;
//using Epitaph.Models;

//namespace Epitaph.Inspectors
//{
//  [CustomEditor(typeof(UnitModel), true)]
//  class UnitInspector : Editor
//  {
//    public override void OnInspectorGUI()
//    {
//      DrawDefaultInspector();

//      var unit = ((UnitModel)target);

//      if (unit.Tile != null)
//      {
//        GUILayout.Label("Current tile: " + unit.Tile.X + ", " + unit.Tile.Y);
//      }
//      else
//      {
//        GUILayout.Label("No tile");
//      }

//      EditorUtility.SetDirty(target);
//    }
//  }
//}