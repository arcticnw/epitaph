﻿//using UnityEditor;
//using UnityEngine;
//using System.Collections;
//using Epitaph.Controllers;
//using Epitaph.Models;

//namespace Epitaph.Inspectors
//{
//  [CustomEditor(typeof(GameController), true)]
//  class GameControllerInspector : Editor
//  {
//    int offsetSteps = 20;

//    bool tileProperties = true;

//    public override void OnInspectorGUI()
//    {
//      DrawDefaultInspector();

//      var scene = ((GameController)target);

//      GUILayout.Space(4);

//      if (scene.selectedTile != null)
//      {
//        GUILayout.Label("Selected tile:");
//        TileInfo(scene.selectedTile);
//      }
//      else
//      {
//        GUILayout.Label("No selected tile");
//      }

//      if (scene.selectedUnit != null)
//      {
//        GUILayout.Label("Selected unit:");
//        UnitInfo(scene.selectedUnit.Model);
//      }
//      else
//      {
//        GUILayout.Label("No selected unit");
//      }

//      EditorUtility.SetDirty(target);
//    }

//    private void TileInfo(TileModel tile)
//    {
//      //tileProperties = EditorGUILayout.Foldout(tileProperties, string.Format("Tile {0}, {1}", tile.X, tile.Y));
//      GUILayout.Label(string.Format("Tile {0}, {1}", tile.X, tile.Y));
//      if (tileProperties)
//      {
//        GUILayout.BeginVertical(new GUIStyle() { padding = new RectOffset(offsetSteps, 0, 0, 0) });
//        GUILayout.Label("Elevation: " + tile.Elevation);
//        if (tile.Units.Count != 0)
//        {
//          GUILayout.Label("Units: ");
//          GUILayout.BeginVertical(new GUIStyle() { padding = new RectOffset(offsetSteps, 0, 0, 0) });
//          foreach (var u in tile.Units)
//          {
//            UnitInfo(u);
//          }
//          GUILayout.EndVertical();
//        }
//        GUILayout.EndVertical();
//      }
//    }
//    private void UnitInfo(UnitModel u)
//    {
//      GUILayout.Label(string.Format("{0} ({1})", u.name, u.Faction == null ? "factionless" : u.Faction.Name));
//      GUILayout.BeginVertical(new GUIStyle() { padding = new RectOffset(offsetSteps, 0, 0, 0) });
//      GUILayout.Label(string.Format("HP:{0} MP:{1} AP:{2}", u.Health, u.ActionPoints, u.MovementPoints));
//      GUILayout.EndVertical();
//    }
//  }
//}