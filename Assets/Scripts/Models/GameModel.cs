﻿using System.Collections.Generic;
using UnityEngine;

namespace Epitaph.Models
{
  class GameModel
  {
    public WorldModel World;

    public List<FactionModel> Factions = new List<FactionModel>();
    public FactionModel PlayerFaction;

    public void Initialize()
    {
      Factions.Add(PlayerFaction = new FactionModel() { Color = new Color(128 / 255f, 168 / 255f, 255 / 255f), Name = "PlayerFaction" });
      Factions.Add(new FactionModel() { Color = new Color(255 / 255f, 60 / 255f, 43 / 255f), Name = "EnemyFaction" });
    }

    public void EndTurn()
    {
      foreach (var fact in Factions)
      {
        foreach (var unit in fact.Units)
        {
          unit.EndTurn();
        }
      }
    }
  }
}