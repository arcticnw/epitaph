﻿using System.Collections.Generic;
using Epitaph.Controllers;
using Epitaph.Misc;
using UnityEngine;

namespace Epitaph.Models
{
  class StructureFoundationModel : StructureModel
  {
    public const string PrefabName = "Models/Structures/Floor";
    public static readonly Point[] Signature = new Point[] { new Point(0, 0) };

    private const int TextureVariants = 4;

    /// <summary>
    /// Tile that is anchor for the set of foundation blocks that this block is part of. Used for aligning foundations into regular shapes (so we avoid zig-zag placement where one block is 1 tile off.)
    /// </summary>
    public TileModel AnchorTile;

    protected override Point[] signature { get { return Signature; } }
    public override string prefabName { get { return PrefabName; } }

    public override void Initialize()
    {
      Debug.Assert(AnchorTile != null, "Anchor tile not set.");

      base.Initialize();
    }

    public override void InitializeGraphics(StructureController controller)
    {
      var n = Random.Range(0, TextureVariants);
      controller.transform.GetComponentInChildren<MeshRenderer>().material.mainTextureScale = new Vector2(1 / (float)TextureVariants, 1);
      controller.transform.GetComponentInChildren<MeshRenderer>().material.mainTextureOffset = new Vector2(n / (float)TextureVariants, 0);
      var mr = controller.transform.GetComponentInChildren<MeshRenderer>();
      base.InitializeGraphics(controller);
    }
  }
}
