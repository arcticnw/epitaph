﻿using System;
using System.Collections.Generic;
using Epitaph.Misc;

namespace Epitaph.Models
{
  class StructureWallModel : StructureModel
  {
    public const string PrefabName = "Models/Structures/Wall";
    public static readonly Point[] Signature = new Point[] { new Point(0, 0), new Point(-1, 0), new Point(1, 0), new Point(0, 1), new Point(-1, 1), new Point(1, 1) };

    protected override Point[] signature { get { return Signature; } }
    public override string prefabName { get { return PrefabName; } }
  }
}
