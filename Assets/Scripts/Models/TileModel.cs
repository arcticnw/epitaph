﻿using System;
using System.Collections.Generic;
using System.Linq;
using Epitaph.Misc;
using UnityEngine;

namespace Epitaph.Models
{
  [System.Diagnostics.DebuggerDisplay("Tile[{X}, {Y}]")]
  class TileModel
  {
    public int X { get; private set; }
    public int Y { get; private set; }
    public Point Position { get { return new Point(X, Y); } }

    public byte Elevation { get; set; }
    public bool IsWater { get; set; }

    public bool IsFoundation { get; set; }

    public MapModel Region { get; private set; }

    #region Internal
    public bool IsBorder { get; set; }

    private static int[,] surrounding = new int[,] { { -1, -1 }, { -1, 0 }, { -1, 1 }, { 0, -1 }, { 0, 1 }, { 1, -1 }, { 1, 0 }, { 1, 1 }};
    #endregion

    public List<UnitModel> Units = new List<UnitModel>();
    public List<StructureModel> Structures = new List<StructureModel>();

    public TileModel(int x, int y, MapModel region)
    {
      X = x;
      Y = y;
      Region = region;
    }

    public void AssignUnit(UnitModel u)
    {
      Debug.Assert(u.Tile == null);
      Debug.Assert(!Units.Contains(u));
      Units.Add(u);
    }
    public void UnassignUnit(UnitModel u)
    {
      Units.Remove(u);
    }
    public void AssignStructure(StructureModel s) // add support for multitile units
    {
      Debug.Assert(!Structures.Contains(s));
      Structures.Add(s);
    }
    public void UnassignStructure(StructureModel s)
    {
      Structures.Remove(s);
    }

    public int GetFlatDistance(TileModel target)
    {
      Queue<TileModel> queue = new Queue<TileModel>();
      HashSet<TileModel> visited = new HashSet<TileModel>();

      queue.Enqueue(this);
      queue.Enqueue(null);

      int distSoFar = 0;

      while (queue.Count > 0)
      {
        TileModel dq = queue.Dequeue();
        if (dq == null)
        {
          distSoFar++;
          queue.Enqueue(null);
          continue;
        }

        if (dq == target) return distSoFar;

        visited.Add(dq);

        foreach (var nei in dq.GetNeighbours())
        {
          if (visited.Contains(nei)) continue;

          queue.Enqueue(nei);
        }
      }

      return -1;
    }

    public TileModel GetRelated(OrientationEnum orientation)
    {
      return GetRelated(Globals.OrientationToPoint(orientation));
    }
    public TileModel GetRelated(Point point)
    {
      var np = point + Position;

      if (np.x < 0 || np.y < 0) return null;
      if (np.x >= Region.SizeX || np.y >= Region.SizeZ) return null;
      return Region[np];
    }
    public IEnumerable<TileModel> GetNeighbours(bool? cardinalOnly = null)
    {
      if (!cardinalOnly.HasValue || cardinalOnly.Value)
        for (int i = 0; i < 4; i++)
        {
          var t = GetRelated(Globals.CardinalOrientations[i]);
          if (t != null)
          yield return t;
        }

      if (!cardinalOnly.HasValue || !cardinalOnly.Value)
        for (int i = 0; i < 4; i++)
        {
          var t = GetRelated(Globals.DiagonalOrientations[i]);
          if (t != null)
            yield return t;
        }
    }
    public IEnumerable<TileModel> GetNeighbours(int maxDist)
    {
      HashSet<TileModel> visited = new HashSet<TileModel>();
      Queue<TileModel> queue = new Queue<TileModel>();
      visited.Add(this);
      queue.Enqueue(this);
      queue.Enqueue(null);

      int distSoFar = 1;

      while (queue.Count > 1 && distSoFar <= maxDist)
      {
        var dq = queue.Dequeue();

        if (dq == null)
        {
          queue.Enqueue(null);
          distSoFar++;
          continue;
        }

        foreach (var t in dq.GetNeighbours())
        {
          if (visited.Contains(t)) continue;
          visited.Add(t);
          queue.Enqueue(t);
          yield return t;
        }
      }
    }

    public Func<PathFindingStruct, PathFindingStruct.Reaction> InlandOnly()
    {
      return (pfs) => pfs.Tile.IsWater ? PathFindingStruct.Reaction.Impassable : 0;
    }
    public Func<PathFindingStruct, PathFindingStruct.Reaction> NonHostileOnly(FactionModel f)
    {
      return (pfs) => pfs.Tile.Units.Any(u => u.Faction != f) ? PathFindingStruct.Reaction.Impassable : 0;
    }
    public Func<PathFindingStruct, PathFindingStruct.Reaction> HostileOnly(FactionModel f)
    {
      return (pfs) => pfs.Tile.Units.All(u => u.Faction == f) ? PathFindingStruct.Reaction.SkipOver : 0;
    }

    public IEnumerable<TileModel> GetTilesWithin(float dist, Func<PathFindingStruct, float> tileEval = null, params Func<PathFindingStruct, PathFindingStruct.Reaction>[] tileBehavior)
    {
      // by default, flat pathing
      if (tileEval == null)
        tileEval = pfs => 1f;

      // by default, no special tile exceptions
      if (tileBehavior == null)
        tileBehavior = new Func<PathFindingStruct, PathFindingStruct.Reaction>[] { pfs => 0 };

      HashSet<TileModel> visited = new HashSet<TileModel>();
      var heap = new BinaryHeap<PathFindingStruct>(PathFindingStruct.Comparer.Instance);
      heap.Insert(new PathFindingStruct() { Tile = this, Value = 0, Distance = 0 });

      while (heap.Count > 0)
      {
        PathFindingStruct dq = heap.RemoveRoot();

        if (visited.Contains(dq.Tile))
          continue;
        visited.Add(dq.Tile);

        bool forceAccept = false;
        bool skipOver = false;
        bool impass = false;

        foreach (var bhv in tileBehavior)
        {
          var react = bhv(dq);
          switch (react)
          {
            case PathFindingStruct.Reaction.ForceAccept:
              forceAccept = true;
              break;
            case PathFindingStruct.Reaction.Impassable:
              impass = true;
              break;
            case PathFindingStruct.Reaction.SkipOver:
              skipOver = true;
              break;
          }
        }

        if (impass && skipOver) skipOver = false; // impass > skip
        if ((impass || skipOver) && forceAccept) forceAccept = false; // impass/skip > accept

        if (impass)
          continue;

        if (dq.Value > dist)
        {
          if (forceAccept)
            yield return dq.Tile;
          break;
        }

        if (!skipOver)
          yield return dq.Tile;

        foreach (var item in dq.Tile.GetNeighbours())
        {
          var pfs = new PathFindingStruct() { Tile = item, Distance = dq.Distance + 1 };
          float pathValue = tileEval(pfs);
          pfs.Value = dq.Value + pathValue;
          heap.Insert(pfs);
        }
      }
    }

    public float GetDistance(TileModel target, Func<PathFindingStruct, float> tileEval = null, params Func<PathFindingStruct, PathFindingStruct.Reaction>[] tileBehavior)
    {
      // by default, flat pathing
      if (tileEval == null)
        tileEval = pfs => 1f;

      // by default, no special tile exceptions
      if (tileBehavior == null)
        tileBehavior = new Func<PathFindingStruct, PathFindingStruct.Reaction>[] { pfs => 0 };

      HashSet<TileModel> visited = new HashSet<TileModel>();
      var heap = new BinaryHeap<PathFindingStruct>(PathFindingStruct.Comparer.Instance);
      heap.Insert(new PathFindingStruct() { Tile = this, Value = 0, Distance = 0 });

      while (heap.Count > 0)
      {
        PathFindingStruct dq = heap.RemoveRoot();

        if (dq.Tile == target)
          return dq.Distance;

        if (visited.Contains(dq.Tile))
          continue;
        visited.Add(dq.Tile);

        bool forceAccept = false;
        bool skipOver = false;
        bool impass = false;

        foreach (var bhv in tileBehavior)
        {
          var react = bhv(dq);
          switch (react)
          {
            case PathFindingStruct.Reaction.ForceAccept:
              forceAccept = true;
              break;
            case PathFindingStruct.Reaction.Impassable:
              impass = true;
              break;
            case PathFindingStruct.Reaction.SkipOver:
              skipOver = true;
              break;
          }
        }

        if (impass && skipOver) skipOver = false; // impass > skip
        if ((impass || skipOver) && forceAccept) forceAccept = false; // impass/skip > accept

        if (impass)
          continue;

        foreach (var item in dq.Tile.GetNeighbours())
        {
          var pfs = new PathFindingStruct() { Tile = item, Distance = dq.Distance + 1 };
          float pathValue = tileEval(pfs);
          pfs.Value = dq.Value + pathValue;
          heap.Insert(pfs);
        }
      }

      return -1;
    }
  }
}
