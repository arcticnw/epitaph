﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Epitaph.Models
{
  class AttackModel
  {
    public string Name;

    public int ActionPointsCost;
    public float HitChance;
    public List<DamageModel> Damage;

    public AttackModel(string Name, float HitChance, int ApCost, params DamageModel[] damages)
    {
      this.Name = Name;
      this.HitChance = HitChance;
      this.ActionPointsCost = ApCost;
      this.Damage = new List<Models.DamageModel>(damages);
    }
  }
}
