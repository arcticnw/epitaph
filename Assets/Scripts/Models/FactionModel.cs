﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Epitaph.Controllers;

namespace Epitaph.Models
{
  class FactionModel
  {
    public string Name;
    public List<UnitModel> Units = new List<UnitModel>();
    public Color Color;

    public void AssignUnit(UnitModel unit)
    {
      Debug.Assert(unit.Faction == null);
      unit.Faction = this;
      Units.Add(unit);

      unit.DirtyBit = true;
    }

    public void UnassignUnit(UnitModel unit)
    {
      Units.Remove(unit);
      unit.Faction = null;

      unit.DirtyBit = true;
    }
  }
}
