﻿using System;
using System.Collections.Generic;
using Epitaph.Misc;
using UnityEngine;

namespace Epitaph.Models
{
  class SquareMesh
  {
    private const float MODIFIER_ELEVATION = 1;

    private const int VERTEX_LIMIT_PER_MESH = 65520;
    private const float BASE_ELEVATION = -0.5f;

    private const float SQUARE_OVERLAP = 0.05f;
    private const float SQUARE_UV_OVERLAP = -0.1f;

    // 2^16 is the hard limit, go down the the nearest multiple of HEX_VERTIECS and HEX_VERTICES + HEX_WALL_VERTICES

    public const float STEP_X = 3;
    public const float STEP_Z = 3;

    // square:

    public const int SQUARE_VERTICES = 4;
    private const int SQUARE_INDICES = 6;

    public static readonly Vector2[] SquareVertices = new Vector2[] { new Vector2(0, 0), new Vector2(0, STEP_Z), new Vector2(STEP_X, STEP_Z), new Vector2(STEP_X, 0) };
    public static readonly Vector2[] SquareCenters = new Vector2[] { new Vector2(0, STEP_Z / 2f), new Vector2(STEP_X / 2f, STEP_Z), new Vector2(STEP_X, STEP_Z / 2f), new Vector2(STEP_X / 2f, 0) };
    public static readonly Vector3 SquareCenter = new Vector3(STEP_X / 2f, 0, STEP_Z / 2f);

    private static readonly Vector2[] squareVerticesDirection = new Vector2[] { new Vector2(-1, -1), new Vector2(-1, 1), new Vector2(1, 1), new Vector2(1, -1) };
    ///// <summary>
    ///// Ordering of the edges of a tile
    ///// </summary>
    public static List<OrientationEnum> EdgeDirections = new List<OrientationEnum>() { OrientationEnum.Left, OrientationEnum.Up, OrientationEnum.Right, OrientationEnum.Down, };

    // order of the indices of triangles in the square
    // if editing, remember counterclockwise ordering of vertices
    private static readonly int[] squareIndicesOrdering = new int[] {
      0, 1, 2,
      0, 2, 3
    };

    // walls:

    private const int SQUARE_WALL_VERTICES = 4 * SQUARE_VERTICES;
    private const int SQUARE_WALL_INDICES = 6 * SQUARE_VERTICES;

    //  for every vertex of the square there are 4 vertices for the wall:
    //  0, 1 - top and bottom vertices for the counter-clockwise rectangle
    //  2, 3 - top and bottom vertices for the clockwise rectangle
    // 
    //         vertex
    //       of the square
    //           v
    //         
    //  [0]   [2][0]    [2]
    //   |------||------|
    //   |      ||      |
    //   |------||------|
    //  [1]   [3][1]    [3]
    //
    // first number defines which vertex of the square, the second number defines which "subvertex" of the vertex of the square (see the diagram above)
    // if editing, remember counterclockwise ordering of vertices
    private static readonly int[,] wallIndicesOrders = new int[,] {
        {0, 0}, {0, 1}, {1, 2}, {1, 2}, {0, 1}, {1, 3}, // N
        {1, 0}, {1, 1}, {2, 2}, {2, 2}, {1, 1}, {2, 3}, // W
        {2, 0}, {2, 1}, {3, 2}, {3, 2}, {2, 1}, {3, 3}, // S
        {3, 0}, {3, 1}, {0, 2}, {0, 2}, {3, 1}, {0, 3} // ES
    };


    private int size_x;
    private int size_z;

    private bool squaresSet;
    private bool wallsSet;
    private bool uvsSet;

    private int meshCount;

    // vertices are limited to 65k per mesh, so if we need more then that, split it into multiple meshes
    private List<List<Vector3>> vertices;
    private List<List<int>> triangleSquares;
    private List<List<int>> triangleWalls;
    private int[,] baseIndices;
    private List<List<Vector2>> uv;
    private List<int> squareStored;

    private MapModel terrain;

    public SquareMesh(int size_x, int size_z)
    {
      this.size_x = size_x;
      this.size_z = size_z;
    }

    public int GetMeshCount()
    {
      return meshCount;
    }

    public void ApplyMesh(GameObject obj, int idx)
    {
      Debug.Assert(obj != null);
      Debug.Assert(squaresSet);
      Debug.Assert(idx >= 0 && idx < meshCount);

      var mesh = GetMesh(idx);

      var mf = obj.GetComponent<MeshFilter>();
      var mc = obj.GetComponent<MeshCollider>();

      mf.mesh = mesh;
      mc.sharedMesh = mesh;
    }

    public Mesh GetMesh(int idx)
    {
      Debug.Assert(idx >= 0 && idx < meshCount);
      Debug.Assert(squaresSet);

      var mesh = new Mesh()
      {
        name = "squaremesh:" + (idx + 1) + "/" + meshCount
      };
      //mesh.MarkDynamic(); // todo: test performance

      if (wallsSet)
        mesh.subMeshCount = 2;

      mesh.SetVertices(vertices[idx]);
      mesh.SetTriangles(triangleSquares[idx], 0);
      if (wallsSet)
        mesh.SetTriangles(triangleWalls[idx], 1);

      if (uvsSet)
        mesh.SetUVs(0, uv[idx]);

      mesh.RecalculateNormals();

      return mesh;
    }

    public void SetTerrain(MapModel terrain)
    {
      this.terrain = terrain;
    }
    private float GetElevation(int x, int z)
    {
      return GetElevation(x, z, terrain);
    }
    private static float GetElevation(int x, int z, MapModel region)
    {
      return region == null ? 0 : GetElevation(region[x, z].Elevation);
    }
    private static float GetElevation(byte rawElevation)
    {
      return rawElevation * MODIFIER_ELEVATION;
    }

    public static Vector2 GetPosition(int x, int z)
    {
      return new Vector2(x * STEP_X, z * STEP_Z);
    }
    public static Vector3 GetPosition(int x, int z, MapModel region)
    {
      var xz = GetPosition(x, z);
      var elev = GetElevation(x, z, region);
      return new Vector3(xz.x, elev, xz.y);
    }
    public static Vector3 GetPosition(int x, int z, byte rawElevation)
    {
      var xz = GetPosition(x, z);
      var elev = GetElevation(rawElevation);
      return new Vector3(xz.x, elev, xz.y);
    }

    public void CreateSquaresOnly()
    {
      Debug.Assert(!squaresSet, "Squares already created");
      squaresSet = true;

      int squareNum = size_x * size_z;
      int vertexNumber = squareNum * SQUARE_VERTICES;

      meshCount = (int)Math.Ceiling(vertexNumber / (double)VERTEX_LIMIT_PER_MESH);

      triangleSquares = new List<List<int>>(meshCount);
      vertices = new List<List<Vector3>>(meshCount);
      baseIndices = new int[size_x, size_z];
      squareStored = new List<int>(meshCount);

      int verticesLeft = vertexNumber;
      //int submeshIdx = 0;

      while (verticesLeft > 0)
      {
        var subVertices = (int)Math.Min(VERTEX_LIMIT_PER_MESH, verticesLeft);
        verticesLeft -= subVertices;
        var subSquares = subVertices / SQUARE_VERTICES;
        Debug.Assert(subSquares == (subVertices / (float)SQUARE_VERTICES), "sanity check; number of vertices should be divisible by # of vertices per square");

        int subIndices = squareNum * SQUARE_VERTICES;
        squareStored.Add(subSquares);

        triangleSquares.Add(new List<int>(subIndices));
        vertices.Add(new List<Vector3>(vertexNumber));
        //submeshIdx++;
      }

      //Debug.Assert(submeshIdx == meshCount, "sanity check");

      int submeshIdx = 0;
      var maxSquaresPerMesh = VERTEX_LIMIT_PER_MESH / SQUARE_VERTICES;

      for (int x = 0; x < size_x; x++)
        for (int z = 0; z < size_z; z++)
        {
          int hex = x * size_z + z;
          if (hex >= (maxSquaresPerMesh * (submeshIdx + 1))) submeshIdx++;
          CreateSquare(x, z, submeshIdx);
        }
    }
    private void CreateSquare(int x, int z, int submesh)
    {
      int baseIdx = vertices[submesh].Count;
      baseIndices[x, z] = submesh * VERTEX_LIMIT_PER_MESH + baseIdx;

      var xz = GetPosition(x, z);
      float elev = GetElevation(x, z);

      for (int i = 0; i < SQUARE_VERTICES; i++)
        vertices[submesh].Add(new Vector3(xz.x + SquareVertices[i].x + squareVerticesDirection[i].x * SQUARE_OVERLAP, elev, xz.y + SquareVertices[i].y + squareVerticesDirection[i].y * SQUARE_OVERLAP));

      for (int i = 0; i < SQUARE_INDICES; i++)
        triangleSquares[submesh].Add(baseIdx + squareIndicesOrdering[i]);
    }
    public void CreateSquaresAndWalls()
    {
      Debug.Assert(!squaresSet, "Squares already created");
      wallsSet = true;
      squaresSet = true;

      int squareNum = size_x * size_z;
      int vertexNumber = squareNum * SQUARE_VERTICES;
      int vertexNumberWalls = squareNum * SQUARE_WALL_VERTICES;

      meshCount = (int)Math.Ceiling(((vertexNumber + vertexNumberWalls) / (double)VERTEX_LIMIT_PER_MESH));
      //Debug.LogFormat("vertexnum: {0}, vertexnumwalls: {1}", vertexNumber, vertexNumberWalls);

      triangleSquares = new List<List<int>>(meshCount);
      triangleWalls = new List<List<int>>(meshCount);
      vertices = new List<List<Vector3>>(meshCount);
      baseIndices = new int[size_x, size_z];
      squareStored = new List<int>(meshCount);

      int verticesLeft = vertexNumber + vertexNumberWalls;

      while (verticesLeft > 0)
      {
        var subVertices = (int)Math.Min(VERTEX_LIMIT_PER_MESH, verticesLeft);
        verticesLeft -= subVertices;
        var subSquares = subVertices / (SQUARE_VERTICES + SQUARE_WALL_VERTICES);
        Debug.Assert(subSquares == (subVertices / (float)(SQUARE_VERTICES + SQUARE_WALL_VERTICES)), "sanity check; number of vertices should be divisible by # of vertices per square");

        int subSquareIndices = squareNum * SQUARE_INDICES;
        int subWallIndices = squareNum * SQUARE_WALL_INDICES;
        squareStored.Add(subSquares);

        //Debug.LogFormat("Vert {0}, submeshidx {1}, meshcount {2}", vertexNumber + vertexNumberWalls, submeshIdx, meshCount);
        //Debug.LogFormat("subhexindices: {0}", subHexIndices);
        //Debug.LogFormat("trihexes: {0}", triangleHexes.Count);

        triangleSquares.Add(new List<int>(subSquareIndices));
        triangleWalls.Add(new List<int>(subWallIndices));
        vertices.Add(new List<Vector3>(vertexNumber));
        //triangleHexes[submeshIdx] = new List<int>(subHexIndices);
        //triangleWalls[submeshIdx] = new List<int>(subWallIndices);
        //vertices[submeshIdx] = new List<Vector3>(vertexNumber);
      }

      int submeshIdx = 0;
      var maxSquaresPerMesh = VERTEX_LIMIT_PER_MESH / (SQUARE_VERTICES + SQUARE_WALL_VERTICES);

      //DateTime now = DateTime.Now;

      for (int x = 0; x < size_x; x++)
      {
        for (int z = 0; z < size_z; z++)
        {
          int square = x * size_z + z;
          if (square >= (maxSquaresPerMesh * (submeshIdx + 1))) submeshIdx++;

          //if (((hex + 1) % 2000) == 0) Debug.Log("Generating mesh ... (" + hex + "/" + hexNumber + ")");

          // int baseSquare = vertices[submeshIdx].Count;
          // Debug.Assert(baseHex + submeshIdx * maxHexesPerMesh == hex * (HEX_VERTICES + HEX_WALL_VERTICES), "sanity check");  -- invalid

          CreateSquare(x, z, submeshIdx);

          int baseSquare = vertices[submeshIdx].Count;

          float elev = GetElevation(x, z);
          var xz = GetPosition(x, z);

          //int hex = vertOffset + 4 * baseIndices[x, z];
          //Debug.LogFormat("vertOffset: {0}, base: {1}, v.c: {2}", vertOffset, baseIndices[x, z], vertices.Count);

          for (int i = 0; i < SQUARE_VERTICES; i++)
            vertices[submeshIdx].AddRange(new Vector3[] {
            new Vector3(xz.x + SquareVertices[i].x, elev - 0.01f, xz.y + SquareVertices[i].y),
            new Vector3(xz.x + SquareVertices[i].x, BASE_ELEVATION, xz.y + SquareVertices[i].y),
            new Vector3(xz.x + SquareVertices[i].x, elev - 0.01f, xz.y + SquareVertices[i].y),
            new Vector3(xz.x + SquareVertices[i].x, BASE_ELEVATION, xz.y + SquareVertices[i].y) });

          //bool skip = true;

          //if (x == 0 || z == 0 || x == size_x - 1 || z == size_z - 1) skip = false;
          //else if (GetElevation(x - 1, z) != elev || GetElevation(x + 1, z) != elev) skip = false;
          //else if (GetElevation(x, z - 1) != elev || GetElevation(x, z + 1) != elev) skip = false;

          //if (!skip)
            for (int i = 0; i < SQUARE_WALL_INDICES; i++)
              triangleWalls[submeshIdx].Add(baseSquare + wallIndicesOrders[i, 0] * 4 + wallIndicesOrders[i, 1]);

        }
        //Debug.LogFormat("{0} / {1}", x, size_x);
      }

      //Debug.LogFormat("Mesh created in: {0} seconds", (DateTime.Now - now).TotalSeconds);
    }

    public void AssignUVs(int spritesInRow = 1, int spritesInColumn = 1, Func<int, int, Vector2> tilePick = null)
    {
      // tilePick: input: [x, z] indexes of the sprite in the texture

      Debug.Assert(!uvsSet, "UVs already set");
      Debug.Assert(squaresSet, "Create hexes first");
      uvsSet = true;

      float max_x = 0;
      float max_z = 0;

      for (int i = 0; i < SQUARE_VERTICES; i++)
      {
        if (SquareVertices[i].x > max_x) max_x = SquareVertices[i].x;
        if (SquareVertices[i].y > max_z) max_z = SquareVertices[i].y;
      }

      if (tilePick == null)
        tilePick = (px, pz) => Vector2.zero;

      uv = new List<List<Vector2>>(meshCount);

      int submeshIdx = 0;
      int maxSquaresPerMesh = 0;
      if (!wallsSet)
        maxSquaresPerMesh = VERTEX_LIMIT_PER_MESH / SQUARE_VERTICES;
      else
        maxSquaresPerMesh = VERTEX_LIMIT_PER_MESH / (SQUARE_VERTICES + SQUARE_WALL_VERTICES);

      for (int i = 0; i < meshCount; i++)
        uv.Add(new List<Vector2>());

      float adjustx = max_x * spritesInRow;
      float adjusty = max_z * spritesInColumn;

      for (int x = 0; x < size_x; x++)
        for (int z = 0; z < size_z; z++)
        {
          int hex = x * size_z + z;
          if (hex >= (maxSquaresPerMesh * (submeshIdx + 1)))
          {
            submeshIdx++;
          }

          var spriteIdx = tilePick(x, z);
          var sprite = new Vector2(spriteIdx.x / spritesInRow, spriteIdx.y / spritesInColumn);

          for (int i = 0; i < SQUARE_VERTICES; i++)
            uv[submeshIdx].Add(
              new Vector2(
                sprite.x + SquareVertices[i].x / adjustx + squareVerticesDirection[i].x * SQUARE_UV_OVERLAP / adjustx,
                sprite.y + SquareVertices[i].y / adjusty + squareVerticesDirection[i].y * SQUARE_UV_OVERLAP / adjusty));


          if (wallsSet)
          {
            for (int i = 0; i < SQUARE_VERTICES; i++)
              uv[submeshIdx].AddRange(new Vector2[] {
              new Vector2(0, 0),
              new Vector2(1, 0),
              new Vector2(0, 1),
              new Vector2(1, 1),
            });
          }
        }
    }

    public static void ChangeElevation(int x, int z, byte newRawElev, List<GameObject> segments, int size_x, int size_z)
    {
      //if (!walls) throw new NotImplementedException();

      int square = x * size_z + z;
      var maxSquaresPerMesh = VERTEX_LIMIT_PER_MESH / (SQUARE_VERTICES + SQUARE_WALL_VERTICES);
      int segmentidx = square / maxSquaresPerMesh;
      var segment = segments[segmentidx];
      var mesh = segment.GetComponent<MeshFilter>().mesh;
      var vertices = mesh.vertices;

      int tileidx = square % maxSquaresPerMesh;
      int baseidx = tileidx * (SQUARE_VERTICES + SQUARE_VERTICES * 4);

      var xz = GetPosition(x, z);
      var elev = GetElevation(newRawElev);

      for (int i = 0; i < SQUARE_VERTICES; i++)
        vertices[baseidx + i] = 
          new Vector3(xz.x + SquareVertices[i].x + squareVerticesDirection[i].x * SQUARE_OVERLAP, elev, xz.y + SquareVertices[i].y + squareVerticesDirection[i].y * SQUARE_OVERLAP);

      for (int i = 0; i < SQUARE_VERTICES; i++)
      {
        var baseidx2 = baseidx + SQUARE_VERTICES * (i + 1);
        vertices[baseidx2 + 0] = new Vector3(xz.x + SquareVertices[i].x, elev - 0.01f, xz.y + SquareVertices[i].y);
        //new Vector3(xz.x + SquareVertices[i].x, BASE_ELEVATION, xz.y + SquareVertices[i].y);
        vertices[baseidx2 + 2] = new Vector3(xz.x + SquareVertices[i].x, elev - 0.01f, xz.y + SquareVertices[i].y);
        //new Vector3(xz.x + SquareVertices[i].x, BASE_ELEVATION, xz.y + SquareVertices[i].y);
      }

      mesh.vertices = vertices;
      mesh.RecalculateNormals();
    }
    public static void ChangeUV(int x, int z, int spritesInRow, int spritesInColumn, Vector2 tile, List<GameObject> segments, int size_x, int size_z)
    {      
      //if (!walls) throw new NotImplementedException();

      int square = x * size_z + z;
      var maxSquaresPerMesh = VERTEX_LIMIT_PER_MESH / (SQUARE_VERTICES + SQUARE_WALL_VERTICES);
      int segmentidx = square / maxSquaresPerMesh;
      var segment = segments[segmentidx];
      var mesh = segment.GetComponent<MeshFilter>().mesh;
      var uv = mesh.uv;

      int tileidx = square % maxSquaresPerMesh;
      int baseidx = tileidx * (SQUARE_VERTICES + SQUARE_VERTICES * 4);

      float max_x = 0;
      float max_z = 0;

      for (int i = 0; i < SQUARE_VERTICES; i++)
      {
        if (SquareVertices[i].x > max_x) max_x = SquareVertices[i].x;
        if (SquareVertices[i].y > max_z) max_z = SquareVertices[i].y;
      }
      float adjustx = max_x * spritesInRow;
      float adjusty = max_z * spritesInColumn;

      var sprite = new Vector2(tile.x / spritesInRow, tile.y / spritesInColumn);

      for (int i = 0; i < SQUARE_VERTICES; i++)
        uv[baseidx + i] =
          new Vector2(
                sprite.x + SquareVertices[i].x / adjustx + squareVerticesDirection[i].x * SQUARE_UV_OVERLAP / adjustx,
                sprite.y + SquareVertices[i].y / adjusty + squareVerticesDirection[i].y * SQUARE_UV_OVERLAP / adjusty);

      mesh.uv = uv;
    }
  }
}