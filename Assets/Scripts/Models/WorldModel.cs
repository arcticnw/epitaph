﻿using System.Collections.Generic;
using UnityEngine;

namespace Epitaph.Models
{
  class WorldModel
  {
    public List<MapModel> Maps = new List<MapModel>();

    public void CreateMap()
    {
      var tex = Resources.Load<Texture2D>("height");
      var r = new MapModel(tex.width, tex.height);
      var elevations = new byte[tex.width, tex.height];

      for (int x = 0; x < tex.width; x++)
      {
        for (int y = 0; y < tex.height; y++)
        {
          var c = tex.GetPixel(x, y);
          var e = ((c.r + c.g + c.b) / 3) * 255;
          elevations[x, y] = (byte)e;
        }
      }

      r.ApplyElevations(elevations);

      Maps.Add(r);
    }
  }
}