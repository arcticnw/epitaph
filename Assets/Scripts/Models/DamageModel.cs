﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Epitaph.Models
{
  class DamageModel
  {
    public enum DamageType
    {
      Physical,
      Bleed,
      Fire
    }

    public int Value;
    public DamageType Type;

    public DamageModel(DamageType Type, int Value)
    {
      this.Type = Type;
      this.Value = Value;
    }
  }
}
