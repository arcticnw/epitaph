﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Epitaph.Controllers;
using UnityEngine;

namespace Epitaph.Models
{
  class UnitModel
  {
    //

    // make getter and setter and call "refresh graphics" method from controller
    public TileModel Tile { get; private set; }
    public FactionModel Faction; //  { get; private set; }

    //

    public int Health = 100; // in range [0, 100], 0 = dead, 100 = ok

    public float MovementPoints = 2; //  { get; private set; }
    public float ActionPoints = 1; //  { get; private set; }

    public bool DirtyBit = false;

    //

    public void EndTurn()
    {
      MovementPoints = 2f;
      ActionPoints += 1;
    }

    public bool CanAttack(UnitModel defender, AttackModel att)
    {
      if (ActionPoints < att.ActionPointsCost)
      {
        // Debug.LogFormat("{0} would fail: out of ap", att.Name);
        return false;
      }
      if (Health <= 0)
      {
        // Debug.LogFormat("{0} would fail: out of health", att.Name);
        return false;
      }

      // todo:
      // if (!range)
      // {
        // Debug.LogFormat("{0} would fail: out of range", att.Name);
        // return false;
      // }

      return true;
    }
    public void Attack(UnitModel defender, AttackModel att)
    {
      // this method should only be called from UnitController

      // todo: roll hit chance

      defender.Health -= att.Damage.Sum(p => p.Value);
      ActionPoints -= att.ActionPointsCost;
    }

    public void MoveTo(TileModel target)
    {
      if (Tile != null)
        Tile.UnassignUnit(this);
      Tile = null;

      if (target != null)
        target.AssignUnit(this);
      Tile = target;

      DirtyBit = true;
    }

    public bool CanWalkTo(TileModel target)
    {
      Debug.Assert(Tile != null, "For movement from invalid position 'MoveTo' instead.");
      Debug.Assert(target != null, "For movement to invalid position 'MoveTo' instead.");

      if (Tile == target)
      {
        Debug.LogFormat("Movement would fail: target tile same as origin tile");
        return false;
      }

      if (target.Units.Count > 0)
      {
        Debug.LogFormat("Movement would fail: target tile has units");
        return false;
      }

      var dist = Tile.GetDistance(target, null, Tile.InlandOnly(), Tile.NonHostileOnly(Faction));
      if (dist > MovementPoints)
      {
        Debug.LogFormat("Movement would fail: MP: {0}, required: {1}", MovementPoints, dist);
        return false;
      }

      return true;
    }
    public void WalkTo(TileModel target)
    {
      var dist = Tile.GetDistance(target, null, Tile.InlandOnly(), Tile.NonHostileOnly(Faction));
      MovementPoints -= dist;

      MoveTo(target);
    }

    public IEnumerable<TileModel> GetMovementTargets()
    {
      return Tile.GetTilesWithin(MovementPoints, null, Tile.InlandOnly(), Tile.NonHostileOnly(Faction)).Where(p => p != Tile);
    }
    public IEnumerable<UnitModel> GetAttackTargets()
    {
      foreach (var t in Tile.GetTilesWithin(2f, null, Tile.InlandOnly(), Tile.HostileOnly(Faction)))
      {
        foreach (var u in t.Units.Where(p => p.Faction != Faction))
        {
          yield return u;
        }
      }
    }

    public void Kill()
    {
      if (Tile != null)
        Tile.UnassignUnit(this);
      if (Faction != null)
        Faction.UnassignUnit(this);
    }
  }
}