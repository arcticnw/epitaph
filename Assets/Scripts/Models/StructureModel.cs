﻿using System.Collections.Generic;
using Epitaph.Misc;
using System.Linq;
using UnityEngine;

namespace Epitaph.Models
{
  abstract class StructureModel
  {
    public abstract string prefabName { get; }
    protected abstract Point[] signature { get; }

    public OrientationEnum Orientation
    {
      get
      {
        return orientation;
      }
      set
      {
        Debug.Assert(Globals.IsCardinal(value));
        orientation = value;
      }
    }
    private OrientationEnum orientation = OrientationEnum.Up;

    public List<TileModel> Tiles = new List<TileModel>();

    public FactionModel Faction;

    public bool DirtyBit;

    public virtual void Initialize()
    {

    }
    public virtual void InitializeGraphics(Controllers.StructureController controller)
    {

    }

    public virtual void MoveTo(TileModel target)
    {
      foreach (var item in Tiles)
      {
        item.UnassignStructure(this);
      }

      List<Point> pts = new List<Point>(signature);
      Globals.RotatePoints(pts, orientation);

      Debug.Assert(
        pts.FirstOrDefault(p => p.x == 0 && p.y == 0) == null ||
        pts.FirstOrDefault(p => p.x == 0 && p.y == 0) == pts.First(),
        "the [0;0] point is in the signature, it has to be first");

      foreach (var pt in pts)
      {
        var t = target.GetRelated(pt);
        t.AssignStructure(this);
        Tiles.Add(t);
      }

      DirtyBit = true;
    }

  }
}
