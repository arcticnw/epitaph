﻿using System;
using System.Collections.Generic;
using System.Linq;
using Epitaph.Misc;

namespace Epitaph.Models
{
  class MapModel
  {
    //

    public int SizeX { get; private set; }
    public int SizeZ { get; private set; }
    public TileModel[,] Tiles { get; private set; }

    public const byte ELEVATION_STEP = 36;

    // indexer

    public TileModel this[int x, int z]
    {
      get
      {
        return Tiles[x, z];
      }
    }
    public TileModel this[Point p]
    {
      get
      {
        return Tiles[p.x, p.y];
      }
    }

    // constructor

    public MapModel(int x, int z)
    {
      SizeX = x;
      SizeZ = z;
      Tiles = new TileModel[x, z];

      for (int ix = 0; ix < x; ix++)
      {
        for (int iz = 0; iz < z; iz++)
        {
          var t = new TileModel(ix, iz, this);
          if (ix == 0 || iz == 0 || ix == x - 1 || iz == z - 1) t.IsBorder = true;
          Tiles[ix, iz] = t;
        }
      }
    }

    //

    public void ApplyElevations(byte[,] elev)
    {
      for (int x = 0; x < SizeX; x++)
      {
        for (int z = 0; z < SizeZ; z++)
        {
          var e = (byte)(Math.Round(elev[x, z] / (float)ELEVATION_STEP)); // (elev[x, z] / ELEVATION_STEP);
          Tiles[x, z].Elevation = e;
          if (e == 0) Tiles[x, z].IsWater = true;
        }
      }
    }
  }
}