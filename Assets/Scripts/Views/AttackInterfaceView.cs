﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Epitaph.Controllers;
using Epitaph.Models;
using Epitaph.ViewModels;
using UnityEngine;
using UnityEngine.UI;

namespace Epitaph.Views
{
  class AttackInterfaceView : MonoBehaviour
  {
    private const string ID_BOX = "box";
    private const string ID_DEFENDER = "defender";
    private const string ID_ATTACKER = "attacker";
    private const string ID_FLAG = "flag";
    private const string ID_UNIT = "unit";
    private const string ID_HPBAR = "hpbar";
    private const string ID_HPBARVALUE = "bar";
    private const string ID_STATS = "stats";
    private const string ID_HEALTH = "hpvalue";
    private const string ID_AP = "apvalue";
    private const string ID_MP = "mpvalue";

    private const string ID_OPTIONSLIST = "optionsview/viewport/content";
    private const string ID_OPTION = "option";
    private const string ID_OPTIONNAME = "optionname";
    private const string ID_APCOST = "apvalue";
    private const string ID_HITCHANCE = "hitvalue";
    private const string ID_DMG = "dmgvalue";
    private const string ID_HIT = "hit";

    private const string ID_CANCEL = "cancel";

    public GameObject defaultOption;

    public void Apply(AttackInterfaceViewModel viewModel)
    {
      // the defaultOption is assigned in editor, but the intellisense doesn't know that and keeps warning that it's never assigned ...
      var x = defaultOption;
      defaultOption = x;

      defaultOption.SetActive(false);

      var box = transform.FindChild(ID_BOX);

      var defenderBox = box.FindChild(ID_DEFENDER);
      FillInUnitData(viewModel.Defender, defenderBox);

      var attackerBox = box.FindChild(ID_ATTACKER);
      FillInUnitData(viewModel.Attacker, attackerBox);

      FlushAttackOptions();

      FillInAttackOptions(viewModel.Attacks);

      box.transform.FindChild(ID_CANCEL).GetComponent<Button>().onClick.AddListener(() => viewModel.CancelCallback());
    }

    private void FillInUnitData(UnitModel u, Transform target)
    {
      target.FindChild(ID_HPBAR).FindChild(ID_HPBARVALUE).GetComponent<Image>().fillAmount = u.Health / 100f;

      var unitPrefab = Resources.Load<GameObject>("Unit");

      var unitModel = unitPrefab.transform.FindChild(UnitController.ID_UNIT);
      var unitModelTexture = unitModel.GetComponent<MeshRenderer>().sharedMaterial.GetTexture(0) as Texture2D;
      var unitSprite = target.transform.FindChild(ID_UNIT);
      var unitSpriteImage = unitSprite.GetComponent<Image>();

      unitSpriteImage.sprite = Sprite.Create(unitModelTexture, new Rect(Vector2.zero, new Vector2(unitModelTexture.width, unitModelTexture.height)), Vector2.zero);

      var flagModel = unitPrefab.transform.FindChild(UnitController.ID_FLAG);
      var flagModelTexture = flagModel.GetComponent<MeshRenderer>().sharedMaterial.GetTexture(0) as Texture2D;
      var flagSprite = target.transform.FindChild(ID_FLAG);
      var flagSpriteImage = flagSprite.GetComponent<Image>();

      flagSpriteImage.sprite = Sprite.Create(flagModelTexture, new Rect(Vector2.zero, new Vector2(flagModelTexture.width, flagModelTexture.height)), Vector2.zero);
      flagSpriteImage.color = u.Faction == null ? Color.white : u.Faction.Color;

      var statsBox = target.FindChild(ID_STATS);

      statsBox.FindChild(ID_HEALTH).GetComponent<Text>().text = u.Health.ToString();
      statsBox.FindChild(ID_AP).GetComponent<Text>().text = u.ActionPoints.ToString();
      statsBox.FindChild(ID_MP).GetComponent<Text>().text = u.MovementPoints.ToString();
    }
    private void FillInAttackOptions(List<AttackViewModel> attacks)
    {
      int c = 0;
      var defaultPosition = defaultOption.GetComponent<RectTransform>().anchoredPosition;
      var optionParent = transform.FindChild(ID_BOX).FindChild(ID_OPTIONSLIST);
      foreach (var att in attacks)
      {
        var attOpt = MakeAttackOption(att);
        attOpt.transform.SetParent(optionParent);
        attOpt.GetComponent<RectTransform>().anchoredPosition = defaultPosition + c++ * new Vector2(0, -72);
      }
    }
    private GameObject MakeAttackOption(AttackViewModel attackVM)
    {
      var attack = attackVM.Attack;

      var attOpt = GameObject.Instantiate(defaultOption);
      attOpt.name = "_" + name;
      attOpt.tag = Globals.RUNTIME_OBJ_TAG;
      attOpt.SetActive(true);

      attOpt.GetComponent<RectTransform>().localScale = Vector3.one;

      attOpt.transform.FindChild(ID_OPTIONNAME).GetComponent<Text>().text = attack.Name;
      attOpt.transform.FindChild(ID_APCOST).GetComponent<Text>().text = attack.ActionPointsCost.ToString();
      attOpt.transform.FindChild(ID_HITCHANCE).GetComponent<Text>().text = string.Format("{0}%", attack.HitChance * 100);

      var sb = new StringBuilder();
      foreach (var dmg in attack.Damage)
      {
        sb.AppendFormat("{1}, {0} pts\n", dmg.Value, dmg.Type);
      }
      attOpt.transform.FindChild(ID_DMG).GetComponent<Text>().text = sb.ToString().TrimEnd(); // TrimEnd for the last \n

      var btn = attOpt.transform.FindChild(ID_HIT).GetComponent<Button>();

      if (attackVM.Available)
      {
        btn.onClick.AddListener(() => attackVM.Callback());
      }
      else
      {
        btn.interactable = false;
      }

      return attOpt;
    }

    private void FlushAttackOptions()
    {
      var box = transform.FindChild(ID_BOX).FindChild(ID_OPTIONSLIST);

      for (int i = 0; i < box.childCount; i++)
      {
        var child = box.GetChild(i);
        if (child.CompareTag(Globals.RUNTIME_OBJ_TAG))
        {
          Globals.Scrap(child.gameObject);
          i--;
        }
      }
    }

    public void Deactivate()
    {
      FlushAttackOptions();
    }
  }
}
