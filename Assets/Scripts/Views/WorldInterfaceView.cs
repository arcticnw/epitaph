﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Epitaph.Controllers;
using Epitaph.Misc;
using Epitaph.Models;
using Epitaph.ViewModels;
using UnityEngine;
using UnityEngine.UI;

namespace Epitaph.Views
{
  class WorldInterfaceView : MonoBehaviour
  {
    private const string ID_BOX = "box";
    private const string ID_SELECT_INFO = "infobox";
    private const string ID_SELECT_IMAGE = "image";
    private const string ID_SELECT_STATS = "stats";
    private const string ID_SELECT_NAME = "title";
    private const string ID_SELECT_FAVOURITE = "favourite";
    private const string ID_SELECT_SWITCH = "switch";


    private const string ID_REGION_NAME = "regionname";
    private const string ID_REGION_IMAGE = "image";

#pragma warning disable 0649 // Field XYZ is never assigned to, and will always have its default value XX
    public GameObject defaultOption;
#pragma warning restore 0649

    private WorldRegionViewModel lastSelected;
    private Dictionary<WorldRegionViewModel, GameObject> regionBoxes = new Dictionary<WorldRegionViewModel, GameObject>();

    public void Apply(WorldInterfaceViewModel vm)
    {
      FlushAttackOptions();

      int c = 0;
      var defaultPosition = defaultOption.GetComponent<RectTransform>().anchoredPosition;
      var optionParent = defaultOption.transform.parent;
      foreach (var item in vm.Regions)
      {
        var region = MakeRegionOption(item);
        region.transform.SetParent(optionParent);
        region.GetComponent<RectTransform>().anchoredPosition = defaultPosition + c++ * new Vector2(0, -92);

        regionBoxes.Add(item, region);
      }

      ChangeSelection(vm.SelectedRegion);
    }

    public void ChangeSelection(WorldRegionViewModel vm)
    {
      if (lastSelected != null)
      {
        var last = regionBoxes[lastSelected];
        var lastbg = last.GetComponent<Image>();
        lastbg.color = defaultOption.GetComponent<Image>().color;
      }

      var box = transform.FindChild(ID_BOX);
      var infobox = box.transform.FindChild(ID_SELECT_INFO);
      var title = infobox.transform.FindChild(ID_SELECT_NAME).GetComponent<Text>();
      title.text = vm == null ? "None" : vm.Name;
      var image = infobox.transform.FindChild(ID_SELECT_IMAGE).GetComponent<Image>();
      image.sprite = vm == null ? null : Sprite.Create(vm.Texture, new Rect(0, 0, vm.Texture.width, vm.Texture.height), Vector2.zero);
      //var stats = transform.FindChild(ID_SELECT_STATS);
      //
      var fav = infobox.transform.FindChild(ID_SELECT_FAVOURITE).GetComponent<Button>();
      fav.onClick.RemoveAllListeners();
      if (vm != null)
        fav.onClick.AddListener(() => vm.Favourite());
      var sw = infobox.transform.FindChild(ID_SELECT_SWITCH).GetComponent<Button>();
      sw.onClick.RemoveAllListeners();
      if (vm != null)
        sw.onClick.AddListener(() => vm.SwitchTo());

      if (vm != null)
      {
        var current = regionBoxes[vm];
        var currentbg = current.GetComponent<Image>();
        currentbg.color = new Color(1, 67 / 255f, 144 / 255f, 100 / 255f);
      }

      lastSelected = vm;
    }

    private GameObject MakeRegionOption(WorldRegionViewModel vm)
    {
      var region = GameObject.Instantiate(defaultOption);
      region.name = "_" + name;
      region.tag = Globals.RUNTIME_OBJ_TAG;
      region.SetActive(true);
      region.GetComponent<RectTransform>().localScale = Vector3.one;

      region.transform.FindChild(ID_REGION_IMAGE).GetComponent<Image>().sprite = Sprite.Create(vm.Texture, new Rect(0, 0, vm.Texture.width, vm.Texture.height), Vector2.zero);
      region.transform.FindChild(ID_REGION_NAME).GetComponent<Text>().text = vm.Name;
      region.GetComponent<Button>().onClick.AddListener(() => vm.Select());

      return region;
    }

    private void FlushAttackOptions()
    {
      ChangeSelection(null);

      var box = defaultOption.transform.parent;

      for (int i = 0; i < box.childCount; i++)
      {
        var child = box.GetChild(i);
        if (child.CompareTag(Globals.RUNTIME_OBJ_TAG))
        {
          child.gameObject.SetActive(false);
#if UNITY_EDITOR
          GameObject.DestroyImmediate(child.gameObject);
#else
          GameObject.Destroy(child.gameObject);
#endif
          i--;
        }
      }
      regionBoxes.Clear();
    }

    public void Deactivate()
    {
      FlushAttackOptions();
    }
  }
}
