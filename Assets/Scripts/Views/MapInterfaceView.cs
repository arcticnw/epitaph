﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Epitaph.Controllers;
using Epitaph.Models;
using UnityEngine;

namespace Epitaph.Views
{
  class MapInterfaceView : MonoBehaviour
  {
    private const string BUILD_MENU = "BuildMenuShown";

    // Controller

    private MapInterfaceController _controller;
    private MapInterfaceController controller
    {
      get
      {
        if (_controller == null) _controller = GetComponent<MapInterfaceController>();
        return _controller;
      }
    }


    #region animator

#pragma warning disable 0649 // Field XYZ is never assigned to, and will always have its default value XX
    public Animator animator;
#pragma warning restore 0649

    #endregion

    //

    public void CycleUnit()
    {
      controller.CycleUnit();
      HideBuildMenu();
    }

    public void EndTurn()
    {
      controller.EndTurn();
      HideBuildMenu();
    }

    public void ChangeRegion()
    {
      controller.InitiateRegionSelect();
      HideBuildMenu();
    }

    public void BuildWall()
    {
      controller.BuildWall();
      HideBuildMenu();
    }

    public void BuildFoundation()
    {
      controller.BuildFoundation();
      HideBuildMenu();
    }

    public void ToggleBuildMenu()
    {
      var menuState = animator.GetBool(BUILD_MENU);
      if (menuState)
        HideBuildMenu();
      else
        ShowBuildMenu();
    }

    public void ShowBuildMenu()
    {
      animator.SetBool(BUILD_MENU, true);
    }
    public void HideBuildMenu()
    {
      animator.SetBool(BUILD_MENU, false);
    }
  }
}
