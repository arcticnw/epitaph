﻿using System.Collections.Generic;
using Epitaph.Models;

namespace Epitaph.Misc
{
  struct PathFindingStruct
  {
    public enum Reaction
    {
      Accept = 0,
      ForceAccept,
      Impassable,
      SkipOver,
    }

    public class Comparer : Comparer<PathFindingStruct>
    {
      public static Comparer Instance = new Comparer();

      public override int Compare(PathFindingStruct x, PathFindingStruct y)
      {
        return x.Value.CompareTo(y.Value);
      }
    }

    public TileModel Tile;
    public int Distance;
    public float Value;
  }
}
