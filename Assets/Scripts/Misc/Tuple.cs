﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Epitaph.Misc
{
  class Tuple<T1, T2>
  {
    class TupleComparer : Comparer<Tuple<T1, T2>>
    {
      private Func<Tuple<T1, T2>, Tuple<T1, T2>, int> comparer;

      public TupleComparer(Func<Tuple<T1, T2>, Tuple<T1, T2>, int> comparer)
      {
        this.comparer = comparer;
      }

      public override int Compare(Tuple<T1, T2> x, Tuple<T1, T2> y)
      {
        return comparer(x, y);
      }
    }

    public T1 Item1;
    public T2 Item2;

    public Tuple(T1 Item1, T2 Item2)
    {
      this.Item1 = Item1;
      this.Item2 = Item2;
    }
  }
}
