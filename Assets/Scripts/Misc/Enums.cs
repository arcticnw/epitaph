﻿namespace Epitaph.Misc
{
  public enum MapDirectionEnum : byte
  {
    NorthWest,
    North,
    NorthEast,
    West,
    Center,
    East,
    SouthWest,
    South,
    SouthEast,
  }

  public enum OrientationEnum : byte
  {
    UpLeft,
    Up,
    UpRight,
    Left,
    Center,
    Right,
    DownLeft,
    Down,
    DownRight,
  }
}