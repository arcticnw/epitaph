﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Epitaph.Misc
{
  public struct Point
  {
    public int x { get; set; }
    public int y { get; set; }

    public Point(int x, int y)
    {
      this.x = x;
      this.y = y;
    }

    public static Point operator +(Point a)
    {
      return a;
    }
    public static Point operator +(Point a, Point b)
    {
      return new Point(a.x + b.x, a.y + b.y);
    }

    public static Point operator -(Point a)
    {
      return new Point(-a.x, -a.y);
    }
    public static Point operator -(Point a, Point b)
    {
      return new Point(a.x - b.x, a.y - b.y);
    }

    public static Point operator *(Point a, int b)
    {
      return new Point(a.x * b, a.y * b);
    }
    public static Point operator /(Point a, int b)
    {
      return new Point(a.x / b, a.y / b);
    }
    public static Point operator %(Point a, int b)
    {
      return new Point(a.x % b, a.y % b);
    }

    public static bool operator ==(Point a, Point b)
    {
      return a.x == b.x && a.y == b.y;
    }
    public static bool operator !=(Point a, Point b)
    {
      return !(a == b);
    }
    public override bool Equals(object obj)
    {
      if (!(obj is Point)) return false;
      return this == (Point)obj;
    }
    public override int GetHashCode()
    {
      return unchecked(x.GetHashCode() * 83 + y.GetHashCode());
    }

    public static implicit operator Vector2(Point a)
    {
      return new Vector2(a.x, a.y);
    }
    public static explicit operator Point(Vector2 a)
    {
      return new Point((int)a.x, (int)a.y);
    }
  }
}
