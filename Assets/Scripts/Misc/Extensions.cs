﻿using UnityEngine;
using System.Collections;

namespace Epitaph.Misc
{
  public static class Extensions
  {
    public static float Length(this Vector2 v)
    {
      return v.magnitude;
      //return Mathf.Sqrt(v.x * v.x + v.y * v.y);
    }
  }
}