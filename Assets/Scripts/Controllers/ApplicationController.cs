﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Epitaph.Controllers
{
  /// <summary>
  /// This is entry point for the game
  /// 
  /// This controller is responsible for generic elements of the applications, such as creating other controllers,
  /// deleting old runtime-created gameobjects, managing different stages of the application.
  /// </summary>
  class ApplicationController : MonoBehaviour
  {
    #region controllers

    private GameController gameController;
    private WorldController worldController;
    private MapController mapController;

    private MapInterfaceController mapInterfaceController;
    private WorldInterfaceController worldInterfaceController;

    #endregion

    #region active controller

    private Dictionary<Type, IMainController> mainControllers;
    private IMainController activeController;

    #endregion

    // *********************************************************************************

    void Start()
    {
      foreach (var item in GameObject.FindGameObjectsWithTag(Globals.RUNTIME_OBJ_TAG))
      {
        Globals.Scrap(item);
      }

      gameController = GameController.CreateController(gameObject);
      worldController = WorldController.CreateController(gameObject);
      mapController = MapController.CreateController(gameObject);
      mapInterfaceController = MapInterfaceController.CreateController(gameObject);
      worldInterfaceController = WorldInterfaceController.CreateController(gameObject);

      gameController.Initialize();
      worldController.Initialize();
      mapController.Initialize();
      mapInterfaceController.Initialize();
      worldInterfaceController.Initialize();

      mainControllers = new Dictionary<Type, IMainController>();
      mainControllers.Add(typeof(MapInterfaceController), mapInterfaceController);
      mainControllers.Add(typeof(WorldInterfaceController), worldInterfaceController);

      mapInterfaceController.gameObject.SetActive(false);
      worldInterfaceController.gameObject.SetActive(false);
      mapController.gameObject.SetActive(false);
      worldController.gameObject.SetActive(false);
      gameController.gameObject.SetActive(false);

      // ** main menu screen

      // vvv-  temporary  -vvv
      gameController.gameObject.SetActive(true);
      worldController.gameObject.SetActive(true);

      gameController.StartGame();

      ActivateInterface(typeof(MapInterfaceController));
    }

    public void ActivateInterface(Type type, object args = null)
    {
      if (activeController != null)
      {
        activeController.Deactivate();
      }

      activeController = mainControllers[type];
      activeController.Activate(args);
    }
  }
}
