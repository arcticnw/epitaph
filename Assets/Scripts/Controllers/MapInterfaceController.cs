﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Epitaph.Misc;
using Epitaph.Models;
using Epitaph.Views;
using UnityEngine;
using EventSystem = UnityEngine.EventSystems.EventSystem;

namespace Epitaph.Controllers
{
  /// <summary>
  /// This controller is responsible for interpreting user input on the map screen.
  /// </summary>
  class MapInterfaceController : MonoBehaviour, IMainController
  {
    private enum ControllerState
    {
      Selection,
      WallPlacing,
      FoundationPlacing,
    }

    private enum PlacementState
    {
      Ok,
      SizeError,
      ElevChange,
      ElevError,
      Snap,
      SnapError,
      Occupied
    }

    #region constants and readonly properties

    public const string INTERFACE_NAME = "Interfaces/MapInterface";

    #endregion

    #region controllers

    private ApplicationController applicationController;

    private GameController gameController;
    private WorldController worldController;
    private MapController mapController;

    private AttackInterfaceController attackIFController;
    public bool IsAttackInterfaceActive { get { return attackIFController != null && attackIFController.gameObject.activeInHierarchy; } }

    #endregion

    #region game state

    private ControllerState state;

    private UnitController selectedUnit { get { return mapController.SelectedUnit; } }
    private TileModel selectedTile { get { return mapController.SelectedTile; } }
    private List<TileModel> selectedTiles { get { return mapController.SelectedTiles; } }
    private bool isTileSelected
    {
      get
      {
        if (selectedTile != null) return true;
        if (selectedTiles != null && selectedTiles.Count > 0) return true;
        return false;
      }
    }
    private MapModel selectedMap { get { return mapController.SelectedMap; } }

    private OrientationEnum selectedEdge;

    #endregion

    #region camera

    private EventSystem eventSystem; // for unity ui
    private Camera mainCamera;
    private Vector3 cameraPanPosition;

    #endregion

    #region view

#pragma warning disable 0649 // Field XYZ is never assigned to, and will always have its default value XX
    private MapInterfaceView view;
#pragma warning restore 0649

    #endregion

    public bool DirtyBit { get; set; }

    // *********************************************************************************

    #region initialization methods

    public static MapInterfaceController CreateController(GameObject parent)
    {
      var obj = (GameObject)Instantiate(Resources.Load(INTERFACE_NAME));
      obj.name = "_" + obj.name;
      obj.tag = Globals.RUNTIME_OBJ_TAG;
      obj.transform.SetParent(parent.transform);

      var controller = obj.GetComponent<MapInterfaceController>();
      return controller;
    }

    public void Initialize()
    {
      applicationController = transform.parent.GetComponentInChildren<ApplicationController>();

      gameController = transform.parent.GetComponentInChildren<GameController>();
      worldController = transform.parent.GetComponentInChildren<WorldController>();
      mapController = transform.parent.GetComponentInChildren<MapController>();
      mainCamera = transform.parent.GetComponentInChildren<Camera>();
      eventSystem = transform.parent.GetComponentInChildren<EventSystem>();

      view = GetComponent<MapInterfaceView>();
    }

    #endregion

    #region update methods

    void Update()
    {
      if (DirtyBit)
      {
        ResetContext();
      }

      switch (state)
      {
        case ControllerState.Selection:
          UpdateSelectionState();
          break;
        case ControllerState.WallPlacing:
          UpdateWallPlacingState();
          break;
        case ControllerState.FoundationPlacing:
          UpdateFoundationPlacingState();
          break;
        default:
          break;
      }

    }

    private void UpdateSelectionState()
    {
      // If the mouse is over GUI, don't actions
      if (!eventSystem.IsPointerOverGameObject())
      {
        UpdateSelectionStateMousePointer();
        UpdateSelectionStateMouseButtons();
      }
    }
    private void UpdateSelectionStateMousePointer()
    {
      Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
      var newTile = mapController.GetMouseoverTile(ray);

      if (newTile == selectedTile)
        return;

      mapController.SelectTile(newTile);
    }
    private void UpdateSelectionStateMouseButtons()
    {
      if (Input.GetMouseButtonUp(0) && !Input.GetKey(KeyCode.LeftShift))
      {
        SelectAction(selectedTile);
      }

      if (Input.GetMouseButtonUp(1))
      {
        bool actionTaken = false;
        if (isTileSelected && selectedUnit != null)
        {
          if (selectedUnit.Model.GetMovementTargets().Contains(selectedTile))
          {
            selectedUnit.GetComponent<UnitController>().WalkTo(selectedTile);
            mapController.SelectUnit(selectedUnit); // reselect unit
            actionTaken = true;
          }

          if (!actionTaken)
          {
            var targets = selectedUnit.Model.GetAttackTargets();
            if (targets != null)
            {
              var target = targets.FirstOrDefault(p => p.Tile == selectedTile);
              if (target != null) // Hostile target
              {
                InitiateAttack(selectedUnit, mapController.GetUnitController(target));
                actionTaken = true;
              }
            }
          }

          if (!actionTaken)
          {
            CancelAttack();
          }
        }
      }

      if (Input.GetMouseButtonDown(2) || Input.GetMouseButtonDown(0) && Input.GetKey(KeyCode.LeftShift))
      {
        HandleMapPanStart();
      }
      else if (Input.GetMouseButton(2) || Input.GetMouseButton(0) && Input.GetKey(KeyCode.LeftShift))
      {
        HandleMapPan();
      }

      HandleMapZoom();
    }

    private void UpdateWallPlacingState()
    {
      // If the mouse is over GUI, don't actions
      if (!eventSystem.IsPointerOverGameObject())
      {
        UpdateWallPlacingStateMousePointer();
        UpdateWallPlacingStateMouseButtons();
      }
    }
    private void UpdateWallPlacingStateMousePointer()
    {
      Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
      var newTile = mapController.GetMouseoverTile(ray);
      var newEdge = newTile == null ? OrientationEnum.Center : mapController.GetEdge(ray, newTile);

      if (newTile == selectedTiles.FirstOrDefault() && newEdge == selectedEdge)
        return;

      if (newTile == null)
      {
        mapController.DeselectTile();
        return;
      }

      selectedEdge = newEdge;

      var candidatePoints = new List<Point>(StructureWallModel.Signature);
      Globals.RotatePoints(candidatePoints, selectedEdge);

      var candidateTiles = new List<TileModel>();

      foreach (var item in candidatePoints)
      {
        var t = newTile.GetRelated(item);
        candidateTiles.Add(t);
      }

      var newTiles = new List<TileModel>();
      var newColors = new List<Color>();

      newTiles.Add(newTile);
      newColors.Add(MapController.tileColorWall);

      foreach (var item in candidateTiles)
      {
        if (item == null)
          continue;
        if (item == newTile)
          continue;

        newTiles.Add(item);
        newColors.Add(MapController.tileColorOk);
      }

      mapController.SelectTiles(newTiles, newColors);
    }
    private void UpdateWallPlacingStateMouseButtons()
    {
      if (Input.GetMouseButtonUp(0) && !Input.GetKey(KeyCode.LeftShift))
      {
        if (isTileSelected && selectedEdge != OrientationEnum.Center)
        {
          mapController.CreateWall(selectedTiles.First(), gameController.Model.PlayerFaction, selectedEdge);
        }
      }

      if (Input.GetMouseButtonDown(2) || Input.GetMouseButtonDown(0) && Input.GetKey(KeyCode.LeftShift))
      {
        HandleMapPanStart();
      }
      else if (Input.GetMouseButton(2) || Input.GetMouseButton(0) && Input.GetKey(KeyCode.LeftShift))
      {
        HandleMapPan();
      }

      HandleMapZoom();
    }

    private void UpdateFoundationPlacingState()
    {
      // If the mouse is over GUI, don't actions
      if (!eventSystem.IsPointerOverGameObject())
      {
        UpdateFoundationPlacingStateMousePointer();
        UpdateFoundationPlacingStateMouseButtons();
      }
    }
    private bool CheckFoundationPlacement(TileModel center, out byte elevation)
    {
      return CheckFoundationPlacement(center, out elevation, null, null);
    }
    private bool CheckFoundationPlacement(TileModel center, List<TileModel> targets, List<Color> colors)
    {
      byte elevation;
      return CheckFoundationPlacement(center, out elevation, targets, colors);
    }
    private bool CheckFoundationPlacement(TileModel center, out byte elevation, List<TileModel> targets, List<Color> colors)
    {
      var candidates = center.GetNeighbours().ToList();
      candidates.Insert(0, center);

      int elevation_ = -1;

      // get outer border tiles of the tiles
      var borderDict = new Dictionary<TileModel, List<TileModel>>();
      var borderTiles = new HashSet<TileModel>();
      foreach (var t in candidates)
        foreach (var tnei in t.GetNeighbours(true))
          if (!borderTiles.Contains(tnei) && !candidates.Contains(tnei))
          {
            borderTiles.Add(tnei);
            if (!borderDict.ContainsKey(t))
              borderDict.Add(t, new List<TileModel>());
            borderDict[t].Add(tnei);
          }

      // find all foundations in those tiles and adapt height
      var foundationTiles = new HashSet<TileModel>();
      foreach (var t in borderTiles)
        if (t.IsFoundation)
        {
          foundationTiles.Add(t);
          if (elevation_ == -1 || t.Elevation == elevation_)
            elevation_ = t.Elevation;
        }

      // if no foundation was found, reset to current tile's elevation
      if (elevation_ == -1)
        elevation_ = center.Elevation;

      bool snapErr = false;
      var foundationBorderingTiles = new HashSet<TileModel>();
      foreach (var item in candidates)
      {
        if (borderDict.ContainsKey(item))
          foreach (var tnei in borderDict[item])
            if (tnei.IsFoundation)
            {
              if (!foundationBorderingTiles.Contains(item))
                foundationBorderingTiles.Add(item);

              var foundationModel = (StructureFoundationModel)tnei.Structures.First(p => p is StructureFoundationModel);

              // if the elevation is different by 1 from the center tile, it has already been adjusted
              if (tnei.Elevation != elevation_)
                snapErr = true;

              // blocks have to be aligned, so we don't have zig-zag constructions (off by 1-2 tiles)
              if (((foundationModel.AnchorTile.X % 3) != (center.X % 3)) ||
                ((foundationModel.AnchorTile.Y % 3) != (center.Y % 3)))
                snapErr = true;
            }
      }


      // helper functions
      Func<TileModel, PlacementState> tilePlacement = (t) =>
      {
        bool snap = false;

        if (candidates.Count != 9)
          return PlacementState.SizeError;

        if (t.IsFoundation)
          return PlacementState.Occupied;

        if (t.IsWater)
          return PlacementState.ElevError;

        if (Math.Abs(t.Elevation - elevation_) > 1)
          return PlacementState.ElevError;

        if (borderDict.ContainsKey(t))
          foreach (var tnei in borderDict[t])
            if (tnei.IsFoundation)
            {
              if (snapErr)
                return PlacementState.SnapError;

              snap = true;

              continue;

              var foundationModel = (StructureFoundationModel)tnei.Structures.First(p => p is StructureFoundationModel);

              // if the elevation is different by 1 from the center tile, it has already been adjusted
              if (tnei.Elevation != elevation_)
                return PlacementState.SnapError;

              // blocks have to be aligned, so we don't have zig-zag constructions (off by 1-2 tiles)
              if (((foundationModel.AnchorTile.X % 3) != (center.X % 3)) ||
                ((foundationModel.AnchorTile.Y % 3) != (center.Y % 3)))
                return PlacementState.SnapError;

              snap = true;
            }

        if (t.Elevation != elevation_)
          return PlacementState.ElevChange;

        if (snap)
          return PlacementState.Snap;

        return PlacementState.Ok;
      };

      Func<PlacementState, Color> pickColor = (state) =>
      {
        switch (state)
        {
          case PlacementState.Ok:
            return MapController.tileColorOk;
          case PlacementState.ElevChange:
            return MapController.tileColorChange;
          case PlacementState.Snap:
            return MapController.tileColorSnap;
          case PlacementState.ElevError:
          case PlacementState.Occupied:
          case PlacementState.SnapError:
            return MapController.tileColorErr;
        }
        return MapController.tileColorOk;
      };

      bool err = false;
      foreach (var item in candidates)
      {
        var tileState = tilePlacement(item);
        if (colors != null && targets != null)
        {
          colors.Add(pickColor(tileState));
          targets.Add(item);
        }

        if (tileState == PlacementState.ElevError ||
          tileState == PlacementState.SizeError ||
          tileState == PlacementState.SnapError ||
          tileState == PlacementState.Occupied)
          err = true;
      }

      elevation = (byte)elevation_;
      return err;
    }
    private void UpdateFoundationPlacingStateMousePointer()
    {
      Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
      var newTile = mapController.GetMouseoverTile(ray);

      if (newTile == selectedTiles.FirstOrDefault())
        return;

      if (newTile == null)
      {
        mapController.DeselectTile();
        return;
      }

      var targetTiles = new List<TileModel>();
      var targetColors = new List<Color>();

      CheckFoundationPlacement(newTile, targetTiles, targetColors);

      mapController.SelectTiles(targetTiles, targetColors);
    }
    private void UpdateFoundationPlacingStateMouseButtons()
    {
      if (Input.GetMouseButtonUp(0) && !Input.GetKey(KeyCode.LeftShift))
      {
        if (isTileSelected)
        {
          byte elevation;
          var baseTile = selectedTiles.First();
          bool err = CheckFoundationPlacement(baseTile, out elevation);

          if (!err)
            foreach (var item in new List<TileModel>(selectedTiles))
              mapController.CreateFoundation(item, baseTile, gameController.Model.PlayerFaction, elevation);
        }
      }

      if (Input.GetMouseButtonDown(2) || Input.GetMouseButtonDown(0) && Input.GetKey(KeyCode.LeftShift))
      {
        HandleMapPanStart();
      }
      else if (Input.GetMouseButton(2) || Input.GetMouseButton(0) && Input.GetKey(KeyCode.LeftShift))
      {
        HandleMapPan();
      }

      HandleMapZoom();
    }

    private void HandleMapPanStart()
    {
      cameraPanPosition = Input.mousePosition;
    }
    private void HandleMapPan()
    {
      var diff = (Input.mousePosition - cameraPanPosition) / 3f; // diff.z is always 0

      var sa = Mathf.Sin(Mathf.Deg2Rad * (mainCamera.transform.rotation.eulerAngles.y + 3 * 90));
      var ca = Mathf.Cos(Mathf.Deg2Rad * (mainCamera.transform.rotation.eulerAngles.y + 3 * 90));

      var rotatedDiff = new Vector2(ca * diff.x - sa * diff.y, sa * diff.x + ca * diff.y);

      var newValue = mainCamera.transform.position - new Vector3(rotatedDiff.x, 0, rotatedDiff.y);

      if (newValue.x < 0) newValue.x = 0;
      if (newValue.z < 0) newValue.z = 0;

      if (newValue.x > SquareMesh.STEP_X * selectedMap.SizeX) newValue.x = SquareMesh.STEP_X * selectedMap.SizeX;
      if (newValue.z > SquareMesh.STEP_Z * selectedMap.SizeZ) newValue.z = SquareMesh.STEP_Z * selectedMap.SizeZ;

      mainCamera.transform.position = newValue;
      cameraPanPosition = Input.mousePosition;
    }
    private void HandleMapZoom()
    {
      if (Input.mouseScrollDelta.y != 0)
      {
        Vector3 newValue = mainCamera.transform.position - new Vector3(0, Input.mouseScrollDelta.y, 0);
        if (newValue.y < 15f) newValue.y = 15;
        mainCamera.transform.position = newValue;
      }
    }

    #endregion

    #region action methods

    private void SelectAction(TileModel tile)
    {
      bool actionTaken = false;
      if (tile != null && tile.Units.Count > 0)
      {
        var unit = tile.Units.FirstOrDefault(u => u.Faction == gameController.Model.PlayerFaction);
        if (unit != null)
        {
          actionTaken = true;
          CancelAttack();
          mapController.SelectUnit(unit);
        }
      }

      if (!actionTaken)
      {
        CancelAttack();
        mapController.DeselectUnit();
      }
    }

    private void InitiateAttack(UnitController attackerUnitController, UnitController defenderUnitController)
    {
      if (attackIFController == null)
        attackIFController = AttackInterfaceController.CreateController(transform);

      attackIFController.gameObject.SetActive(true);
      attackIFController.Initialize(attackerUnitController, defenderUnitController);
    }
    private void CancelAttack()
    {
      if (!IsAttackInterfaceActive)
        return;

      if (attackIFController != null)
      {
        attackIFController.Cancel();
        attackIFController.gameObject.SetActive(false);
      }
    }

    #endregion

    private void ResetContext()
    {
      CancelAttack();
      mapController.ResetContext();
      selectedEdge = OrientationEnum.Center;
      DirtyBit = false;
    }

    #region ui methods

    public void EndTurn()
    {
      gameController.EndTurn();
      DirtyBit = true;
    }
    public void CycleUnit()
    {

    }
    public void BuildWall()
    {
      ResetContext();

      if (state != ControllerState.WallPlacing)
        state = ControllerState.WallPlacing;
      else
        state = ControllerState.Selection;
    }
    public void BuildFoundation()
    {
      ResetContext();

      if (state != ControllerState.FoundationPlacing)
        state = ControllerState.FoundationPlacing;
      else
        state = ControllerState.Selection;
    }

    public void InitiateRegionSelect()
    {
      applicationController.ActivateInterface(typeof(WorldInterfaceController), mapController.SelectedMap);
    }

    #endregion

    #region interface methods

    public void Activate(object args)
    {
      gameObject.SetActive(true);
      mapController.gameObject.SetActive(true);

      if (args == null)
        mapController.LoadRegion(worldController.Model.Maps.First());
      else
        mapController.LoadRegion(args as MapModel);

      state = ControllerState.Selection;
    }
    public void Deactivate()
    {
      mapController.UnloadRegion();

      mapController.gameObject.SetActive(false);
      gameObject.SetActive(false);
    }

    #endregion
  }
}
