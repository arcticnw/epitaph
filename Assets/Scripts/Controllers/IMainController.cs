﻿using UnityEngine;

namespace Epitaph.Controllers
{
  interface IMainController
  {
    void Activate(object args);
    void Deactivate();
  }
}
