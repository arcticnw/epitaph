﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using Epitaph.Models;
using Epitaph.Views;

using EventSystem = UnityEngine.EventSystems.EventSystem;
using UnityEditor;
using System;

namespace Epitaph.Controllers
{
  //[ExecuteInEditMode]
  /// <summary>
  /// This controller is responsible for changes to the state of the GameModel that are direct result
  /// of user input.
  /// </summary>
  class GameController : MonoBehaviour
  {
    #region controllers

    private WorldController worldController;

    #endregion

    #region model

    public GameModel Model;

    #endregion

    // *********************************************************************************

    #region initialization methods

    public static GameController CreateController(GameObject parent)
    {
      var obj = new GameObject("_game", typeof(GameController));
      obj.tag = Globals.RUNTIME_OBJ_TAG;
      obj.transform.SetParent(parent.transform);

      var controller = obj.GetComponent<GameController>();
      //controller.Model = new GameModel();
      return controller;
    }

    public void Initialize()
    {
      worldController = transform.parent.GetComponentInChildren<WorldController>();

      Model = new GameModel();
    }

    #endregion

    #region game methods

    public void StartGame()
    {
      // Create or load the world map
      worldController.ObtainMap();

      // Create and initialize game model
      Model.World = worldController.Model;
      Model.Initialize();
    }

    public void EndTurn()
    {
      Model.EndTurn();
    }

    #endregion
  }
}