﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Epitaph.Misc;
using Epitaph.Models;
using UnityEngine;

namespace Epitaph.Controllers
{
  /// <summary>
  /// This controller is responsible for keeping and modifying the state of the map.
  /// </summary>
  class MapController : MonoBehaviour
  {
    private enum HighlightType
    {
      Mouseover,
      Moveable,
      Attackable,
      Wall,
    }

    #region constants and readonly properties

    public static readonly Color tileColorMouseover = new Color(123 / 255f, 222 / 255f, 255 / 255f);
    public static readonly Color tileColorMoveTo = new Color(198 / 255f, 255 / 255f, 212 / 255f);
    public static readonly Color tileColorWall = new Color(255 / 255f, 213 / 255f, 198 / 255f);
    public static readonly Color tileColorFoundation = new Color(128 / 255f, 128 / 255f, 255 / 255f);

    public static readonly Color tileColorBlock = new Color(255 / 255f, 64 / 255f, 64 / 255f);
    public static readonly Color tileColorTerraform = new Color(64 / 255f, 255 / 255f, 64 / 255f);

    public static readonly Color tileColorOk = new Color(64 / 255f, 255 / 255f, 64 / 255f);
    public static readonly Color tileColorErr = new Color(255 / 255f, 64 / 255f, 64 / 255f);
    public static readonly Color tileColorChange = new Color(64 / 255f, 64 / 255f, 255 / 255f);
    public static readonly Color tileColorSnap = new Color(255 / 255f, 64 / 255f, 255 / 255f);

    public static readonly Vector3 HIGHLIGHT_OFFSET = new Vector3(0, 0.1f, 0);

    #endregion

    #region controllers

    #endregion

    #region properties

    //

    private GameObject mapHexPrototype;
    private List<GameObject> mapHighlightsQueue = new List<GameObject>();

    //

    /// <summary>
    /// This object is a parent of the unit gameobjects
    /// </summary>
    private GameObject mapUnits;
    /// <summary>
    /// This object is a parent of the structure gameobjects
    /// </summary>
    private GameObject mapStructures;
    /// <summary>
    /// This is a list of map segments (meshes) of currently loaded region
    /// </summary>
    private List<GameObject> mapSegments = new List<GameObject>();
    /// <summary>
    /// This object is a parent of the tile highlighting objects
    /// </summary>
    private GameObject mapOverlays;
    /// <summary>
    /// This is a list of tile highlighting objects
    /// </summary>
    private Dictionary<HighlightType, List<GameObject>> mapHighlights;


    //

    /// <summary>
    /// This is a dictionary for back-referencing loaded unit models to their controllers
    /// </summary>
    private Dictionary<UnitModel, UnitController> mapUnitRef = new Dictionary<UnitModel, UnitController>();

    //

    /// <summary>
    /// This is a reference to the currently selected tile if a single tile is being selected (in case of tiles it's the one below the mouse cursor)
    /// </summary>
    public TileModel SelectedTile { get; private set; }
    /// <summary>
    /// This is a list of referenced to the currently selected tiles if multiple tiles are being selected
    /// </summary>
    public List<TileModel> SelectedTiles { get; private set; }
    /// <summary>
    /// This is a reference to the currently selected unit
    /// </summary>
    public UnitController SelectedUnit { get; private set; }
    /// <summary>
    /// This is a reference to the currently loaded region
    /// </summary>
    public MapModel SelectedMap { get; private set; }

    //

    #endregion

    #region internal
    /// <summary>
    /// Materials used for the terrain
    /// </summary>
    private static Material[] materials;
    #endregion

    // *********************************************************************************

    #region initialization methods

    public static MapController CreateController(GameObject parent)
    {
      var obj = new GameObject("_map", typeof(MapController));
      obj.tag = Globals.RUNTIME_OBJ_TAG;
      obj.transform.SetParent(parent.transform);

      var controller = obj.GetComponent<MapController>();
      return controller;
    }

    static MapController()
    {
      materials = new Material[] {
          Resources.Load("TerrainMaterials/GroundMaterial", typeof(Material)) as Material,
          Resources.Load("TerrainMaterials/CliffMaterial", typeof(Material)) as Material
        };
    }

    public void Initialize()
    {
      mapUnits = new GameObject("_units");
      mapUnits.tag = Globals.RUNTIME_OBJ_TAG;
      mapUnits.transform.SetParent(transform);

      mapStructures = new GameObject("_structures");
      mapStructures.tag = Globals.RUNTIME_OBJ_TAG;
      mapStructures.transform.SetParent(transform);

      mapOverlays = new GameObject("_overlays");
      mapOverlays.tag = Globals.RUNTIME_OBJ_TAG;
      mapOverlays.transform.SetParent(transform);

      mapHexPrototype = CreateHighlightPrototype(mapOverlays.transform);

      mapHighlights = new Dictionary<HighlightType, List<GameObject>>();
      foreach (HighlightType item in Enum.GetValues(typeof(HighlightType)))
        mapHighlights.Add(item, new List<GameObject>());

      SelectedTiles = new List<TileModel>();
    }

    private GameObject CreateHighlightPrototype(Transform parent)
    {
      var go = new GameObject("_highlightPrototype", typeof(MeshCollider), typeof(MeshRenderer), typeof(MeshFilter));
      go.transform.SetParent(parent);
      go.tag = Globals.RUNTIME_OBJ_TAG;
      var mr = go.GetComponent<MeshRenderer>();
      mr.sharedMaterial = Resources.Load<Material>("TerrainMaterials/SelectionMaterial");
      mr.sharedMaterial.color = tileColorMouseover;
      mr.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;

      var hm1 = new SquareMesh(1, 1);
      hm1.CreateSquaresOnly();
      hm1.AssignUVs(1, 1);
      hm1.ApplyMesh(go, 0);

      go.SetActive(false);

      return go;
    }

    #endregion

    #region selection methods

    public void SelectTile(TileModel tile)
    {
      SelectTile(tile, tileColorMouseover);
    }
    public void SelectTile(TileModel tile, Color highlightColor)
    {
      DeselectTile();
      SelectedTile = tile;

      if (tile == null)
        return;

      CreateHighlight(HighlightType.Mouseover, tile, highlightColor);
    }
    public void SelectTiles(List<TileModel> tiles, List<Color> highlightColors)
    {
      DeselectTile();
      SelectedTiles = tiles;

      if (tiles == null || tiles.Count == 0)
        return;

      Debug.Assert(tiles.Count == highlightColors.Count);

      for (int i = 0; i < tiles.Count; i++)
      {
        CreateHighlight(HighlightType.Mouseover, tiles[i], highlightColors[i]);
      }
    }
    public void DeselectTile()
    {
      ClearHighlights(HighlightType.Mouseover);
      ClearHighlights(HighlightType.Wall);
      SelectedTile = null;
      SelectedTiles.Clear();
    }
    public void SelectUnit(UnitModel unit)
    {
      SelectUnit(mapUnitRef[unit]);
    }
    public void SelectUnit(UnitController unit)
    {
      DeselectUnit();
      SelectedUnit = unit;

      foreach (var tile in unit.Model.GetMovementTargets())
        CreateHighlight(HighlightType.Moveable, tile, tileColorMoveTo);
      foreach (var target in unit.Model.GetAttackTargets())
        CreateHighlight(HighlightType.Attackable, target.Tile, target.Faction.Color);

    }
    public void DeselectUnit()
    {
      ClearHighlights(HighlightType.Moveable);
      ClearHighlights(HighlightType.Attackable);
      SelectedUnit = null;
    }

    #endregion

    public void ResetContext()
    {
      DeselectTile();
      DeselectUnit();

      ClearHighlights();
    }

    #region highlights

    private GameObject CreateHighlight(HighlightType hl, TileModel t, Color c)
    {
      GameObject obj;
      if (mapHighlightsQueue.Count > 0)
      {
        obj = mapHighlightsQueue.Last();
        mapHighlightsQueue.Remove(obj);
        obj.SetActive(true);
      }
      else
      {
        obj = Instantiate(mapHexPrototype);
        obj.transform.SetParent(mapOverlays.transform);
        obj.tag = Globals.RUNTIME_OBJ_TAG;
        obj.name = "_highlight";
      }

      var mr = obj.GetComponent<Renderer>();
      mr.material.color = c;
      obj.transform.position = SquareMesh.GetPosition(t.X, t.Y, t.Elevation) + HIGHLIGHT_OFFSET;

      mapHighlights[hl].Add(obj);

      return obj;
    }
    private void ClearHighlights(HighlightType hl)
    {
      foreach (var item in mapHighlights[hl])
      {
        item.SetActive(false);
        mapHighlightsQueue.Add(item);
      }
      mapHighlights[hl].Clear();
    }
    private void ClearHighlights()
    {
      if (mapHighlights != null)
        foreach (HighlightType item in Enum.GetValues(typeof(HighlightType)))
          ClearHighlights(item);
    }

    #endregion

    #region tile methods

    public TileModel GetMouseoverTile(Ray ray)
    {
      Debug.Assert(SelectedMap != null);
      Debug.Assert(mapSegments != null && mapSegments.Count > 0);
      //if (mapSegments == null) return null;
      //if (mapSegments.Count == 0) return null;
      //if (selectedRegion == null) return null;

      RaycastHit hitInfo;
      bool found = false;

      for (int i = 0; !found && i < mapSegments.Count; i++)
      {
        // assume every hex is a circle, find nearest one (within reasonable range)
        if (mapSegments[i].GetComponent<MeshCollider>().Raycast(ray, out hitInfo, float.PositiveInfinity))
        {
          var position = transform.InverseTransformPoint(hitInfo.point);

          TileModel bestTile = null;
          float bestDistance = -1;

          int x = (int)(position.x / SquareMesh.STEP_X);
          int z = (int)(position.z / SquareMesh.STEP_Z);
          int range = 2; // don't go further than ~ n hexes away

          for (int ix = x - range; ix < x + range; ix++)
          {
            if (ix < 0) continue;
            if (ix >= SelectedMap.SizeX) continue;
            for (int iz = z - range; iz < z + range; iz++)
            {
              if (iz < 0) continue;
              if (iz >= SelectedMap.SizeZ) continue;

              var tile = SquareMesh.GetPosition(ix, iz, SelectedMap);
              // square distance from the center
              float dist = (position - (tile + SquareMesh.SquareCenter)).sqrMagnitude;

              if (bestDistance == -1 || dist < bestDistance)
              {
                bestTile = SelectedMap[ix, iz];
                bestDistance = dist;
              }
            }
          }

          if (bestDistance != -1)
            return bestTile;
        }
      }
      return null;
    }
    public OrientationEnum GetEdge(Ray ray, TileModel tile)
    {
      Debug.Assert(tile != null);
      Debug.Assert(SelectedMap != null);
      Debug.Assert(mapSegments != null && mapSegments.Count > 0);
      //if (tile == null) return 0;
      //if (mapSegments == null) return 0;
      //if (mapSegments.Count == 0) return 0;
      //if (SelectedRegion == null) return 0;

      // Go through all the segments, find the tile, check which of the centers of the edges is closest to the ray

      RaycastHit hitInfo;
      bool found = false;

      var tilePosition = SquareMesh.GetPosition(tile.X, tile.Y, SelectedMap);

      for (int i = 0; !found && i < mapSegments.Count; i++)
      {
        if (mapSegments[i].GetComponent<MeshCollider>().Raycast(ray, out hitInfo, float.PositiveInfinity))
        {
          var position = transform.InverseTransformPoint(hitInfo.point);

          int bestEdge = 0;
          float bestDistance = -1;

          // Go through all the edges
          for (int j = 0; j < SquareMesh.SQUARE_VERTICES; j++)
          {
            var center2 = SquareMesh.SquareCenters[j];
            var center3 = new Vector3(center2.x, 0, center2.y);
            var dist = (position - (tilePosition + center3)).sqrMagnitude;

            if (dist > 9f) continue;

            if (bestDistance == -1 || bestDistance > dist)
            {
              bestEdge = j;
              bestDistance = dist;
            }
          }

          if (bestDistance != -1)
            return SquareMesh.EdgeDirections[bestEdge];
        }
      }
      return OrientationEnum.Center;
    }
    public UnitController GetUnitController(UnitModel unit)
    {
      return mapUnitRef[unit];
    }

    #endregion

    #region creating methods

    public void CreateWall(TileModel tile, FactionModel faction, OrientationEnum orientation) // ... structure type, region ...
    {
      StructureModel sm = new StructureWallModel();
      sm.Faction = faction;
      sm.MoveTo(tile);
      sm.Orientation = orientation;

      sm.Initialize();
      LoadStructure(sm);
    }
    public void CreateUnit(TileModel tile, FactionModel faction) // ... unit type, region, ...
    {
      UnitModel um = new UnitModel();
      um.Faction = faction;
      um.MoveTo(tile);

      LoadUnit(um);
    }
    public void CreateFoundation(TileModel tile, TileModel anchor, FactionModel faction, byte elev)
    {
      if (tile.Elevation != elev)
      {
        tile.Elevation = elev;
        SquareMesh.ChangeElevation(tile.X, tile.Y, elev, mapSegments, SelectedMap.SizeX, SelectedMap.SizeZ);
      }

      if (!tile.IsFoundation)
      {
        tile.IsFoundation = true;
        SquareMesh.ChangeUV(tile.X, tile.Y, 20, 16, PickSprite(tile.X, tile.Y, SelectedMap), mapSegments, SelectedMap.SizeX, SelectedMap.SizeZ);

        StructureFoundationModel sm = new StructureFoundationModel();
        sm.AnchorTile = anchor;
        sm.Faction = faction;
        sm.MoveTo(tile);
        sm.Orientation = OrientationEnum.Up;
        sm.Initialize();

        LoadStructure(sm);
      }

      if (SelectedTile == tile) SelectedTile = null;
      if (SelectedTiles.Contains(tile)) SelectedTiles.Remove(tile);
    }

    #endregion

    #region loading methods

    // remove
    private Vector2 PickSprite(int x, int z, MapModel map)
    {
      var variance = UnityEngine.Random.Range(0, 3);

      if (map[x, z].IsWater)
        return new Vector2(0 + variance, 4);

      if (map[x, z].IsFoundation)
        return new Vector2(3 + variance, 10);

      if (map[x, z].Elevation < 6)
        return new Vector2(9 + variance, 10);

      return new Vector2(9 + variance, 4);
    }

    public void LoadRegion(MapModel region)
    {
      Debug.Assert(SelectedMap == null);
      //UnloadRegion();

      SelectedMap = region;

      var square = new SquareMesh(SelectedMap.SizeX, SelectedMap.SizeZ);
      square.SetTerrain(region);
      square.CreateSquaresAndWalls();
      square.AssignUVs(20, 16, (px, pz) => PickSprite(px, pz, region));

      for (int i = 0; i < square.GetMeshCount(); i++)
      {
        var subterrain = new GameObject("_submap", typeof(MeshCollider), typeof(MeshFilter), typeof(MeshRenderer));
        subterrain.name = "_" + subterrain.name;
        subterrain.tag = Globals.RUNTIME_OBJ_TAG;
        subterrain.transform.SetParent(transform);
        square.ApplyMesh(subterrain, i);
        subterrain.GetComponent<MeshRenderer>().sharedMaterials = materials;

        mapSegments.Add(subterrain);
      }

      for (int x = 0; x < SelectedMap.SizeX; x++)
      {
        for (int z = 0; z < SelectedMap.SizeZ; z++)
        {
          foreach (var unit in SelectedMap[x, z].Units)
          {
            LoadUnit(unit);
          }
          foreach (var str in SelectedMap[x, z].Structures)
          {
            LoadStructure(str);
          }
        }
      }
    }
    public void UnloadRegion()
    {
      // Reset selections, clear highlights and overlays
      ResetContext();

      // Clear map segments
      foreach (var r in mapSegments)
      {
        Globals.Scrap(r);
      }
      mapSegments.Clear();

      // Clear units
      for (int i = 0; i < mapUnits.transform.childCount; i++)
      {
        Globals.Scrap(mapUnits.transform.GetChild(i).gameObject);
        i--;
      }
      mapUnitRef.Clear();

      // Clear structures
      for (int i = 0; i < mapStructures.transform.childCount; i++)
      {
        Globals.Scrap(mapStructures.transform.GetChild(i).gameObject);
        i--;
      }

      SelectedMap = null;
    }

    private GameObject LoadUnit(UnitModel unit)
    {
      var obj = (GameObject)Instantiate(Resources.Load("Models/Units/Unit"));
      obj.name = "_" + obj.name;
      obj.tag = Globals.RUNTIME_OBJ_TAG;
      obj.transform.SetParent(mapUnits.transform);

      var controller = obj.GetComponent<UnitController>();
      controller.Region = SelectedMap;
      controller.Model = unit;

      mapUnitRef.Add(unit, controller);

      return obj;
    }
    private GameObject LoadStructure(StructureModel structure)
    {
      var obj = (GameObject)Instantiate(Resources.Load(structure.prefabName));
      obj.name = "_" + obj.name;
      obj.tag = Globals.RUNTIME_OBJ_TAG;
      obj.transform.SetParent(mapStructures.transform);

      var controller = obj.GetComponent<StructureController>();
      controller.Region = SelectedMap;
      controller.Model = structure;

      controller.Model.InitializeGraphics(controller);

      return obj;
    }

    #endregion

  }
}
