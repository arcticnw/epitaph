﻿using Epitaph.Misc;
using Epitaph.Models;
using System.Linq;
using UnityEngine;

namespace Epitaph.Controllers
{
  /// <summary>
  /// This controller is responsible for handling all the changes to StructureModel.
  /// </summary>
  class StructureController : MonoBehaviour
  {

    #region models

    public StructureModel Model;
    public MapModel Region;

    #endregion

    // *********************************************************************************

    private void Start()
    {
      InvalidateGraphics();
    }

    #region update

    public void UpdateGraphics()
    {
      // flag, progress, etc goes here, see UnitController

      if (Model.Tiles.FirstOrDefault() != null)
        transform.position = SquareMesh.GetPosition(Model.Tiles[0].X, Model.Tiles[0].Y, Region) + SquareMesh.SquareCenter;

      int yRot = 0;
      switch (Model.Orientation)
      {
        case OrientationEnum.Up:
          yRot = 0;
          break;
        case OrientationEnum.Right:
          yRot = 90;
          break;
        case OrientationEnum.Down:
          yRot = 180;
          break;
        case OrientationEnum.Left:
          yRot = 270;
          break;
      }
      transform.rotation = Quaternion.Euler(0, yRot, 0);

      Model.DirtyBit = false;
    }

    public void InvalidateGraphics()
    {
      Model.DirtyBit = true;
    }

    private void Update()
    {
      if (Model.DirtyBit)
        UpdateGraphics();
    }

    #endregion

    #region structure action

    public void MoveTo(TileModel tile)
    {
      Model.MoveTo(tile);
      InvalidateGraphics();
    }

    #endregion

  }
}
