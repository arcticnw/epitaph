﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using Epitaph.Models;

namespace Epitaph.Controllers
{
  //[ExecuteInEditMode]
  /// <summary>
  /// This controller is responsible for handling all the changes to UnitModel.
  /// </summary>
  class UnitController : MonoBehaviour
  {
    #region constants and readonly properties

    public const string ID_UNIT = "unit";
    public const string ID_FLAG = "flag";
    public const string ID_STATS = "stats";
    public const string ID_HPBAR = "hpbar";
    public const string ID_HPBARVALUE = "bar";

    #endregion

    #region models

    public UnitModel Model;
    public MapModel Region;

    #endregion

    // *********************************************************************************

    void Start()
    {
      InvalidateGraphics();
    }

    #region update methods

    public void UpdateGraphics()
    {
      var stats = transform.FindChild(ID_STATS);
      var hpbar = stats.transform.FindChild(ID_HPBAR);
      var hpbarvalue = hpbar.transform.FindChild(ID_HPBARVALUE);
      hpbarvalue.GetComponent<Image>().fillAmount = Model.Health / 100f;

      var mr = transform.Find(ID_FLAG).GetComponent<MeshRenderer>();
#if UNITY_EDITOR
      // in order to get rid of the following error:
      //   **  Instantiating material due to calling renderer.material during edit mode. This will leak materials into the scene. You most likely want to use renderer.sharedMaterial instead."
      mr.sharedMaterial = new Material(mr.sharedMaterial);
      mr.sharedMaterial.color = Model.Faction == null ? Color.white : Model.Faction.Color;
#else
        mr.material.color = unit.Faction == null ? Color.white : unit.Faction.Color;
#endif

      if (Model.Tile != null)
        transform.position = SquareMesh.GetPosition(Model.Tile.X, Model.Tile.Y, Model.Tile.Elevation);

      Model.DirtyBit = false;
    }

    public void InvalidateGraphics()
    {
      Model.DirtyBit = true;
    }

    void Update()
    {
      if (Model.DirtyBit)
        UpdateGraphics();
    }

    #endregion

    #region unit actions

    public void MoveTo(TileModel tile)
    {
      Model.MoveTo(tile);
      InvalidateGraphics();
    }

    public void WalkTo(TileModel tile)
    {
      if (!Model.CanWalkTo(tile))
      {
        Debug.LogWarning("Invalid move");
        return;
      }

      Model.WalkTo(tile);
      InvalidateGraphics();
    }

    public void Attack(UnitController defenderController, AttackModel att)
    {
      //Debug.LogFormat("{0} is attacking {1}, {2} for total {3} dmg", this, defenderController, att.Name, "x");
      if (!Model.CanAttack(defenderController.Model, att))
      {
        Debug.LogWarning("Invalid move");
        return;
      }

      Model.Attack(defenderController.Model, att);

      InvalidateGraphics();
      defenderController.InvalidateGraphics();
    }

    #endregion

  }
}