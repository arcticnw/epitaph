﻿using UnityEngine;
using System.Collections;
using Epitaph.Models;

namespace Epitaph.Controllers
{
  class WorldController : MonoBehaviour
  {

    #region model

    public WorldModel Model;

    #endregion

    // *********************************************************************************

    #region initialization methods

    public static WorldController CreateController(GameObject parent)
    {
      var obj = new GameObject("_world", typeof(WorldController));
      obj.tag = Globals.RUNTIME_OBJ_TAG;
      obj.transform.SetParent(parent.transform);

      var controller = obj.GetComponent<WorldController>();
      controller.Model = new WorldModel();
      return controller;
    }

    //

    public void Initialize()
    {
    }

    #endregion

    #region map methods

    public void ObtainMap()
    {
      Model.CreateMap();
    }

    #endregion
  }
}