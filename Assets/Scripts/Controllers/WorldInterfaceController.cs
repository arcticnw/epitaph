﻿using UnityEngine;
using System.Collections;
using Epitaph.Views;
using Epitaph.Models;
using System.Linq;
using Epitaph.ViewModels;
using System;

namespace Epitaph.Controllers
{
  class WorldInterfaceController : MonoBehaviour, IMainController
  {
    #region constants and readonly properties

    public const string INTERFACE_NAME = "Interfaces/WorldInterface";

    #endregion

    #region controllers

    private ApplicationController applicationController;

    private WorldController worldController;

    #endregion

    #region view

    private WorldInterfaceView view;

    #endregion

    // *********************************************************************************

    #region initialization methods

    public static WorldInterfaceController CreateController(GameObject parent)
    {
      var obj = (GameObject)Instantiate(Resources.Load(INTERFACE_NAME));
      obj.name = "_" + obj.name;
      obj.tag = Globals.RUNTIME_OBJ_TAG;
      obj.transform.SetParent(parent.transform);

      var controller = obj.GetComponent<WorldInterfaceController>();
      return controller;
    }

    public void Initialize()
    {
      var x = worldController; // warning silencing
      worldController = x;

      applicationController = transform.parent.GetComponentInChildren<ApplicationController>();
      worldController = transform.parent.GetComponentInChildren<WorldController>();

      view = GetComponent<WorldInterfaceView>();
    }

    #endregion

    #region ui methods

    private void SwitchTo(MapModel region)
    {
      applicationController.ActivateInterface(typeof(MapInterfaceController), region);
    }
    private void Favourite(MapModel region)
    {

    }
    private void Select(WorldRegionViewModel regionvm)
    {
      view.ChangeSelection(regionvm);
    }

    #endregion

    #region interface methods

    public void Activate(object args)
    {
      var lastRegion = args as MapModel;

      gameObject.SetActive(true);

      var worldvm = new WorldInterfaceViewModel();

      int i = 0;

      foreach (var region in worldController.Model.Maps)
      {
        i++;
        Texture2D regionImage = new Texture2D(region.SizeX, region.SizeZ) { name = "region image" };
        for (int x = 0; x < region.SizeX; x++)
        {
          for (int z = 0; z < region.SizeZ; z++)
          {
            var h = region[x, z].Elevation;
            Color c = new Color(0, 74 / 255f, 127 / 255f);
            if (h > 0)
            {
              c = Color.Lerp(new Color(91 / 255f, 130 / 255f, 94 / 255f), new Color(121 / 255f, 173 / 255f, 126 / 255f), (float)h);
            }

            regionImage.SetPixel(x, z, c);
          }
        }
        regionImage.Apply();

        var regionvm = new WorldRegionViewModel()
        {
          Texture = regionImage,
          Name = "Region #" + i
        };

        var _hold = region; // lambda encapsulation

        regionvm.SwitchTo = () => SwitchTo(_hold);
        regionvm.Favourite = () => Favourite(_hold);
        regionvm.Select = () => Select(regionvm);

        worldvm.Regions.Add(regionvm);

        if (region == lastRegion)
          worldvm.SelectedRegion = regionvm;
      }

      view.Apply(worldvm);
    }
    public void Deactivate()
    {
      view.Deactivate();
      gameObject.SetActive(false);
    }

    #endregion
  }
}