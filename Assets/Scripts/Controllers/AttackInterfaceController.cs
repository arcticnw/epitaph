﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Epitaph.Models;
using Epitaph.ViewModels;
using Epitaph.Views;
using UnityEngine;

namespace Epitaph.Controllers
{
  /// <summary>
  /// This user interface is responsible for creating the attack sequence.
  /// </summary>
  class AttackInterfaceController : MonoBehaviour
  {
    #region constants and readonly properties

    public const string INTERFACE_NAME = "Interfaces/AttackInterface";

    #endregion

    #region controllers

    private MapInterfaceController mapInterfaceController;

    #endregion

    public List<AttackModel> dummyAttacks = new List<Models.AttackModel>()
      {
        new Models.AttackModel("Melee", 0.95f, 1, new DamageModel(DamageModel.DamageType.Physical, 50)),
        new Models.AttackModel("Backstab", 0.65f, 2, new DamageModel(DamageModel.DamageType.Physical, 30), new DamageModel(DamageModel.DamageType.Bleed, 80)),
        new Models.AttackModel("Aimed shot", 0.3f, 2, new DamageModel(DamageModel.DamageType.Physical, 85)),
        new Models.AttackModel("Molotov", 0.8f, 2, new DamageModel(DamageModel.DamageType.Fire, 40))
      };

    #region view

#pragma warning disable 0649 // Field XYZ is never assigned to, and will always have its default value XX
    private AttackInterfaceView view;
#pragma warning restore 0649

    #endregion

    // *********************************************************************************

    #region initialization methods

    public static AttackInterfaceController CreateController(Transform parent)
    {
      var aiobj = (GameObject)Instantiate(Resources.Load(INTERFACE_NAME));
      aiobj.name = "_" + aiobj.name;
      aiobj.tag = Globals.RUNTIME_OBJ_TAG;
      aiobj.transform.SetParent(parent);

      return aiobj.GetComponent<AttackInterfaceController>();
    }

    public void Initialize(UnitController attacker, UnitController defender)
    {
      mapInterfaceController = transform.parent.GetComponent<MapInterfaceController>();

      var aiVM = new AttackInterfaceViewModel();

      aiVM.CancelCallback = Cancel;

      aiVM.Attacker = attacker.Model;
      aiVM.Defender = defender.Model;

      foreach (var att in dummyAttacks)
      {
        var aVM = new AttackViewModel();

        aVM.Attack = att;
        aVM.Available = attacker.Model.CanAttack(defender.Model, att);

        var _att = att;
        aVM.Callback = () => ExecuteAttack(attacker, defender, _att);

        aiVM.Attacks.Add(aVM);
      }

      view.Apply(aiVM);
    }

    #endregion

    #region interface methods

    public void Cancel()
    {
      Hide();
    }
    public void Hide()
    {
      view.Deactivate();
      gameObject.SetActive(false);
    }

    private void ExecuteAttack(UnitController attacker, UnitController defender, AttackModel attack)
    {
      // int val = attack.Damage.Sum(p => p.Value);
      // Debug.LogFormat("{0} is attacking {1}, {2} for total {3} dmg", attacker, defender, attack.Name, val);
      attacker.Attack(defender, attack);
      Hide();
      mapInterfaceController.DirtyBit = true;
    }

    #endregion
  }
}
