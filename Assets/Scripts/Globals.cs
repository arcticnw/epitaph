﻿using UnityEngine;
using System;
using Epitaph.Misc;
using System.Collections.Generic;

namespace Epitaph
{
  public static class Globals
  {
    // this value has to mirror the tag in editor
    public const string RUNTIME_OBJ_TAG = "_runtime_";

    public static readonly OrientationEnum[] CardinalOrientations = new OrientationEnum[] { OrientationEnum.Up, OrientationEnum.Down, OrientationEnum.Left, OrientationEnum.Right };
    public static readonly OrientationEnum[] DiagonalOrientations = new OrientationEnum[] { OrientationEnum.UpLeft, OrientationEnum.UpRight, OrientationEnum.DownLeft, OrientationEnum.DownRight };

    static Globals()
    {
      TagHelper.AddTag(RUNTIME_OBJ_TAG);
    }

    public static void Scrap(GameObject go, bool assets = true)
    {
      if (go == null) return;
#if UNITY_EDITOR
      GameObject.DestroyImmediate(go, assets);
#else
      item.gameObject.SetActive(false);
      GameObject.Destroy(item, true);
#endif
    }

    public static void MeasureTime(string name, Action action)
    {
      DateTime now = DateTime.Now;
      action();
      Debug.LogFormat("{1}: {0} seconds", (DateTime.Now - now).TotalSeconds, name);
    }

    public static OrientationEnum OrientationToCompass(MapDirectionEnum dir)
    {
      return (OrientationEnum)(byte)dir;
    }
    public static MapDirectionEnum CompassToOrientation(OrientationEnum ori)
    {
      return (MapDirectionEnum)(byte)ori;
    }

    public static void RotatePoints(List<Point> points, OrientationEnum ori)
    {
      if (!IsCardinal(ori)) throw new NotImplementedException();

      if (ori == OrientationEnum.Up) return;

      for (int i = 0; i < points.Count; i++)
      {
        Point p = points[i];
        int tmp;
        switch (ori)
        {
          case OrientationEnum.Right:
            tmp = p.x;
            p.x = p.y;
            p.y = -tmp;
            break;
          case OrientationEnum.Left:
            tmp = p.x;
            p.x = -p.y;
            p.y = tmp;
            break;
          case OrientationEnum.Down:
            p.x = -p.x;
            p.y = -p.y;
            break;
        }
        points[i] = p;
      }
    }

    public static Point OrientationToPoint(OrientationEnum ori)
    {
      int x = 0;
      int y = 0;
      switch (ori)
      {
        case OrientationEnum.UpLeft:
          x--;
          y++;
          break;
        case OrientationEnum.Up:
          y++;
          break;
        case OrientationEnum.UpRight:
          x++;
          y++;
          break;
        case OrientationEnum.Left:
          x--;
          break;
        case OrientationEnum.Center:
          break;
        case OrientationEnum.Right:
          x++;
          break;
        case OrientationEnum.DownLeft:
          x--;
          y--;
          break;
        case OrientationEnum.Down:
          y--;
          break;
        case OrientationEnum.DownRight:
          x++;
          y--;
          break;
        default:
          break;
      }

      return new Point(x, y);
    }
    public static OrientationEnum PointToOrientation(Point pt)
    {
      if (pt.x < 0) // left
      {
        if (pt.y < 0) // down
          return OrientationEnum.DownLeft;
        else if (pt.y > 0) // up
          return OrientationEnum.UpLeft;
        else
          return OrientationEnum.Left;
      }
      else if (pt.x > 0) // right
      {
        if (pt.y < 0) // down
          return OrientationEnum.DownRight;
        else if (pt.y > 0) // up
          return OrientationEnum.UpRight;
        else
          return OrientationEnum.Right;
      }
      else
      {
        if (pt.y < 0) // down
          return OrientationEnum.Down;
        else if (pt.y > 0) // up
          return OrientationEnum.Up;
        else
          return OrientationEnum.Center;
      }
    }

    public static OrientationEnum GetOpposite(OrientationEnum ori)
    {
      switch (ori)
      {
        case OrientationEnum.UpLeft:
          return OrientationEnum.DownRight;
        case OrientationEnum.Up:
          return OrientationEnum.Down;
        case OrientationEnum.UpRight:
          return OrientationEnum.DownLeft;
        case OrientationEnum.Left:
          return OrientationEnum.Right;
        case OrientationEnum.Right:
          return OrientationEnum.Left;
        case OrientationEnum.DownLeft:
          return OrientationEnum.UpRight;
        case OrientationEnum.Down:
          return OrientationEnum.Up;
        case OrientationEnum.DownRight:
          return OrientationEnum.UpLeft;
      }
      return OrientationEnum.Center;
    }
    public static OrientationEnum[] GetOrthogonal(OrientationEnum ori)
    {
      switch (ori)
      {
        case OrientationEnum.UpLeft:
          return new OrientationEnum[] { OrientationEnum.UpRight, OrientationEnum.DownLeft };
        case OrientationEnum.Up:
          return new OrientationEnum[] { OrientationEnum.Right, OrientationEnum.Left };
        case OrientationEnum.UpRight:
          return new OrientationEnum[] { OrientationEnum.UpLeft, OrientationEnum.DownRight };
        case OrientationEnum.Left:
          return new OrientationEnum[] { OrientationEnum.Up, OrientationEnum.Down };
        case OrientationEnum.Right:
          return new OrientationEnum[] { OrientationEnum.Up, OrientationEnum.Down };
        case OrientationEnum.DownLeft:
          return new OrientationEnum[] { OrientationEnum.UpLeft, OrientationEnum.DownRight };
        case OrientationEnum.Down:
          return new OrientationEnum[] { OrientationEnum.Left, OrientationEnum.Right };
        case OrientationEnum.DownRight:
          return new OrientationEnum[] { OrientationEnum.DownLeft, OrientationEnum.UpRight };
      }
      return new OrientationEnum[] { OrientationEnum.Center, OrientationEnum.Center };
    }

    public static OrientationEnum[] GetNeighbours()
    {
      return new OrientationEnum[] {
        OrientationEnum.Up, OrientationEnum.Down, OrientationEnum.Left, OrientationEnum.Right,
        OrientationEnum.UpLeft, OrientationEnum.UpRight, OrientationEnum.DownLeft, OrientationEnum.DownRight};
    }

    public static bool IsCardinal(OrientationEnum ori)
    {
      return ori == OrientationEnum.Up || ori == OrientationEnum.Down || ori == OrientationEnum.Left || ori == OrientationEnum.Right;
    }
    public static bool IsDiagonal(OrientationEnum ori)
    {
      return ori == OrientationEnum.UpLeft || ori == OrientationEnum.UpRight || ori == OrientationEnum.DownLeft || ori == OrientationEnum.DownRight;
    }
  }
}