﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Epitaph.Models;
using UnityEngine;

namespace Epitaph.ViewModels
{
  class WorldRegionViewModel
  {
    public Texture2D Texture;
    public string Name;

    public Action Select;
    public Action Favourite;
    public Action SwitchTo;
  }
}
