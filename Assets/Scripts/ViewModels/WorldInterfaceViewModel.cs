﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Epitaph.Models;
using UnityEngine;

namespace Epitaph.ViewModels
{
  class WorldInterfaceViewModel
  {
    public List<WorldRegionViewModel> Regions = new List<WorldRegionViewModel>();

    public WorldRegionViewModel SelectedRegion = null;
  }
}
