﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Epitaph.Models;

namespace Epitaph.ViewModels
{
  class AttackInterfaceViewModel
  {
    public Action CancelCallback;

    public UnitModel Attacker;
    public UnitModel Defender;

    public List<AttackViewModel> Attacks = new List<AttackViewModel>();
  }
}
